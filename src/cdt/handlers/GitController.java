package cdt.handlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;

public class GitController {

    private String localPath, remotePath;
    private Repository localRepo;
    private Git git;

    public GitController(){
    	
    }
    
    public GitController(String localPath, String remotePath){
    	this.localPath = localPath;
    	this.remotePath = remotePath;
    }
    
    public void init() throws IOException {
        localPath = "C:\\Users\\bruno\\Desktop\\proje\\teste git";
        remotePath = "https://github.com/brunomecca/articles.git";
        localRepo = new FileRepository(localPath + "/.git");
        git = new Git(localRepo);
    }

    public void testCreate() throws IOException {
        Repository newRepo = new FileRepository(localPath + ".git");
        newRepo.create();
    }

    public static void cloneRepository(String remotePath, String localPath) throws IOException, GitAPIException {
        Git git = Git.cloneRepository().setURI(remotePath)
                .setDirectory(new File(localPath)).call();
        
		Repository repo = git.getRepository();
		
		RevWalk walk = new RevWalk(repo);
		ObjectId commitId = ObjectId.fromString( "ab49343b08c933e32de8de78132649f9560a3727" );
		RevCommit commit = walk.parseCommit(commitId);
		
		
        
    }

    public void testAdd() throws IOException, GitAPIException {
        File myfile = new File(localPath + "/myfile");
        myfile.createNewFile();
        git.add().addFilepattern("myfile").call();
    }

    public void testCommit() throws IOException, GitAPIException,
            JGitInternalException {
        git.commit().setMessage("Added myfile").call();
    }

    public void testPush() throws IOException, JGitInternalException,
            GitAPIException {
        git.push().call();
    }

    public void testTrackMaster() throws IOException, JGitInternalException,
            GitAPIException {
        git.branchCreate().setName("master")
                .setUpstreamMode(SetupUpstreamMode.SET_UPSTREAM)
                .setStartPoint("origin/master").setForce(true).call();
    }

    public void testPull() throws IOException, GitAPIException {
        git.pull().call();
    }
    
    public void test(){
    	Git git;
		try {
			git = Git.cloneRepository().setURI("https://github.com/westes/flex.git")
					.setDirectory(new File("C:\\Users\\bruno\\Desktop\\proje\\ic\\test git tag")).call();
			
	    	final List<Ref> tagRefs;
	    	tagRefs = git.tagList().call();
	    	
	    	for(Ref ref : tagRefs) {
	            String name = ref.getName();
	            String name2 = ref.getObjectId().getName();
	            System.out.println(name);
	            System.out.println(name2);
	            if(name.contains("2-5-35"))
	            	git.checkout().setCreateBranch( true ).setName( "new-branch" ).setStartPoint( name2 ).call();
	    	}
	    	
		} catch (InvalidRemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	
    	
    }
    
}
