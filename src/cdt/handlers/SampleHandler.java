package cdt.handlers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.dom.IPDOMManager;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorMacroDefinition;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.index.IIndex;
import org.eclipse.cdt.core.index.IIndexManager;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.cdt.core.settings.model.CSourceEntry;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.cdt.core.settings.model.ICSettingEntry;
import org.eclipse.cdt.core.settings.model.ICSourceEntry;
import org.eclipse.cdt.internal.core.dom.parser.c.CASTTypedefNameSpecifier;
import org.eclipse.cdt.managedbuilder.core.BuildException;
import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.core.IManagedBuildInfo;
import org.eclipse.cdt.managedbuilder.core.IManagedProject;
import org.eclipse.cdt.managedbuilder.core.IToolChain;
import org.eclipse.cdt.managedbuilder.core.ManagedBuildManager;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import analysis.Dependency;
import analysis.ID;
import analysis.Link;
import analysis.core.Function;
import analysis.core.ProgramElement;
import analysis.core.Project;
import metrics.Metrics;
import tree.Opt;
import tree.Variable;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
@SuppressWarnings("restriction")
public class SampleHandler extends AbstractHandler {
	
	private static String PROJECT = "";
	private static String PROJECTDIRECTORY = "";

	/* 
	 * NOTES:

	 */
	
	// What is the Runtime Workspace path?
	private static final String RUNTIME_WORKSPACE_PATH = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString() + File.separator;
	
	// It keeps the C types.
	private List<String> types = new ArrayList<String>();
	
	//It keeps the directive macros
	private List<String> macrosNotToInclude = new ArrayList<String>();
	
	// It keeps the macros defined.
	private List<String> macros = new ArrayList<String>();
	
	// It keeps of files in the SRC folder.
	private List<String> filesInSrc;

	private String toFolder, downloadPath;
	
	private boolean download = false;
	
	//private List<DataResult> dataResults = new ArrayList<DataResult>();
	
	/**
	 * The constructor.
	 */
	public SampleHandler() {
		
	}

	public void filesInAnalysis(String sourceFolder) throws IOException{
		 File[] files = new File(sourceFolder).listFiles();
	        for(File file: files){
	            if(file.isDirectory()){
	            	System.out.println(file.getAbsolutePath().replace("\\", "/"));
	                filesInAnalysis(file.getAbsolutePath());
	            } else {
	                if(file.getName().trim().endsWith(".c") || file.getName().trim().endsWith(".h")){
	                	Files.copy(file.toPath(), (new File(toFolder +"\\"+ file.getName())).toPath(), StandardCopyOption.REPLACE_EXISTING);
	                }
	            }
	        }
	}
	
	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		//start();
		try {
			startdiff();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//test();
		
		MessageDialog.openInformation(
				window.getShell(),
				"Syntax Error Analyzer",
				"Done! Check plaftorm.h and include/stubs.h");
		return null;
	}
	
	private void test(){
		Tests test = new Tests();
		test.test();
	}
	
	private void settrue() {
		download = true;
	}
	private void startdiff() throws Exception {
		long startTime = System.nanoTime();
		BufferedReader reader = setReader();
		List<String> projects = new ArrayList<String>();
		List<String> projectDirectory = new ArrayList<String>();
		String[] versions = null;
		int numberOfProjects = 0;
		try {
			numberOfProjects = Integer.parseInt(reader.readLine());
			versions = new String[numberOfProjects];
			for(int i = 0 ; i < numberOfProjects ; i++){
				String projectString[] = reader.readLine().split(" ");
				projectDirectory.add(projectString[0]);
				versions[i] = projectString[1];
				projects.add(projectString[2]);
			}
			closeReader(reader);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("number of projects "+numberOfProjects );
		System.out.println("versions "+versions[0]);
		System.out.println("projects size "+projects.size());
		
		//setando a lista de lista de links onde serão armazenadas todas as dependencias das versões analisadas
		Metrics.allLinksFromDependenciesOfAllVersions = new ArrayList<List<Link>>();
		Metrics.allDependenciesAndLinksDefined = new ArrayList<List<Dependency>>();
		Metrics.allDependenciesOfAllVersions = new ArrayList<Dependency>();
		
		for(int i = 0 ; i < numberOfProjects; i++){
			String versionsToAnalyze[] = versions[i].split(",");
			for(int j = 0 ; j < versionsToAnalyze.length ; j++){
				SampleHandler.PROJECTDIRECTORY = projectDirectory.get(i);
				SampleHandler.PROJECT = projects.get(i);
				String projectFolder = SampleHandler.PROJECT;
				
				System.out.println("SampleHandler.PROJECT "+SampleHandler.PROJECT);
				System.out.println("SampleHandler.PROJECTDIRECTORY "+SampleHandler.PROJECTDIRECTORY);
				
				//para testes
				//String folderName = "download" + i + "-" + versionsToAnalyze[j];
				
				String folderName = createDownloadFolder(i+1, versionsToAnalyze[j]);
				downloadProject(SampleHandler.PROJECTDIRECTORY, versionsToAnalyze[j]);
				createProjectFolder();
				moveFiles();
				createCProject(folderName);
				SampleHandler.PROJECT = folderName;
				
				//gerando stubs.h e platform.h
				//boolean download para ter certeza de analisar somente a versao do la�o atual

				String localOfFiles = (SampleHandler.RUNTIME_WORKSPACE_PATH + folderName).replace("\\", "/");
				System.out.println(localOfFiles);
				System.out.println("OPA: " + SampleHandler.PROJECTDIRECTORY);
	

				this.analyzeFilesInSrc();
				
				Project project;
				File platform = new File(localOfFiles + "\\platform.h");
				if (platform.length() == 0) {
					project = new Project(localOfFiles + "\\analysis", localOfFiles + "\\include\\stubs.h");
				} else {
					project = new Project(localOfFiles+"\\analysis", localOfFiles+"\\platform.h");
				}
				
				
				project.analyze();
				
				long elapsedTime = System.nanoTime() - startTime;
				System.out.println("Analysis ended in " + TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS) + " seconds.");
				
			}
			
		}
		
		//escrevendo no arquivo dependencies.txt
		createDependenciesTxt();
		
	} 
	
	public void createDependenciesTxt(){
		try {
			FileWriter arq = new FileWriter(SampleHandler.RUNTIME_WORKSPACE_PATH + "\\dependencies.txt");
			PrintWriter gravarArq = new PrintWriter(arq);
			
			//variaveis de controle de versão
			int i = 1;
			//escrevendo dados
			for(List<Dependency> dpsCurrentVersion : Metrics.allDependenciesAndLinksDefined){
				gravarArq.println("Version " + i);
				
				//mostrar novas e mortas dependencias
				
				//verificando se já passou da primeira versão
				List<Dependency> newDependencies = new ArrayList<Dependency>();
				List<Dependency> deadDependencies = new ArrayList<Dependency>();
				if(i > 1){ //
					newDependencies = getAllNewDependencies(dpsCurrentVersion);
					gravarArq.println("	New dependencies: ");
					
					for(Dependency newDependency : newDependencies){
						gravarArq.println(" 		"+newDependency.variability1.getName() + " " +newDependency.variability2.getName());
					}
					
					deadDependencies = getAllDeadDependencies(dpsCurrentVersion);
					gravarArq.println("	Dead dependencies: ");
					
					for(Dependency deadDependency : deadDependencies){
						gravarArq.println(" 		"+deadDependency.variability1.getName() + " " +deadDependency.variability2.getName());
					}
					
				}
				//se não passou, então as dependencias são todas novas
				else{
					gravarArq.println("	New dependencies: ");
					showDependencies(dpsCurrentVersion, gravarArq);
					gravarArq.println("	Dead dependencies: ");
					
					//adicionando na lista de todas as dependencias do sistema
					for(Dependency dpCurrentVersion : dpsCurrentVersion){
						Metrics.allDependenciesOfAllVersions.add(dpCurrentVersion);
					}
					
					//na primeira versão não tem dependencias mortas
				}
				
				gravarArq.println();
				gravarArq.println(" 	Number of dependencies: " + dpsCurrentVersion.size());
				for(Dependency dp : dpsCurrentVersion){
					gravarArq.println(" 		"+dp.variability1.getName() + " " +dp.variability2.getName());

					gravarArq.println(" 		Total number of links: " + dp.linksFromThisDependency.size());
					int values[] = totalNumberOfUniqueLinks(dp.linksFromThisDependency);
					gravarArq.println("		Total number of unique links: " + values[0]);
					gravarArq.println("		Total number of distinct links: " + values[1]);
					
					for(Link link: dp.linksFromThisDependency){
						//mostrando links
						showLinks(link, gravarArq);
					}
					
					//definindo densidade de mudanças nos links
					if(i > 1){
						//segunda+ versão
						List<Link> newLinks = new ArrayList<Link>();
						List<Link> deadLinks = new ArrayList<Link>();
						boolean achou = false;
						for(Dependency dpToFind : Metrics.allDependenciesOfAllVersions){
							if(dpToFind.variability1.getName().equals(dp.variability1.getName()) &&
									dpToFind.variability2.getName().equals(dp.variability2.getName())){
								achou = true;
								//comparar os links
								for(Link linkFromCurrentDp : dp.linksFromThisDependency){
									if(!dpToFind.linksFromThisDependency.contains(linkFromCurrentDp)){
										newLinks.add(linkFromCurrentDp);
										
									}
								}
								
								for(Link linkFromDpToFind : dpToFind.linksFromThisDependency){
									if(!dp.linksFromThisDependency.contains(linkFromDpToFind)){
										deadLinks.add(linkFromDpToFind);
									}
								}
							}
							
						}
						if(!achou){
							gravarArq.println("		Total number of new links: " + dp.linksFromThisDependency.size());
							gravarArq.println("		Total number of removed links: " + 0);
						}
						else{
							gravarArq.println("		Total number of new links: " + newLinks.size());
							gravarArq.println("		Total number of removed links: " + deadLinks.size());
						}
					}
					else{
						//primeira versão
						gravarArq.println("		Total number of new links: " + dp.linksFromThisDependency.size());
						gravarArq.println("		Total number of removed links: " + 0);
						
					}
					gravarArq.println();
					
				}//fim for mostrando as dps
				
				//incremento da versão
				i++;
				for(Dependency newDependency : newDependencies){
					Metrics.allDependenciesOfAllVersions.add(newDependency);
				}
				for(Dependency deadDependency : deadDependencies){
					Metrics.allDependenciesOfAllVersions.remove(deadDependency);
				}
				for(Dependency dpCurrentVersion : dpsCurrentVersion){
					if(!Metrics.allDependenciesOfAllVersions.contains(dpCurrentVersion)){
						Metrics.allDependenciesOfAllVersions.add(dpCurrentVersion);
					}
				}
				
			}

			arq.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showLinks(Link link, PrintWriter gravarArq){
		Variable v;
		Function f;
		if(link.caller.id == ID.Variable){
			v = (Variable) link.caller;
			if(link.unique){
				gravarArq.print("*");
			}
			gravarArq.print(" 			" + v.getName()+ "(var)" + " " + v.getPresenceCondition() + " and ");
		}
		else if(link.caller.id == ID.Function){
			f = (Function) link.caller;
			boolean entrou = false;
			if(link.unique){
				gravarArq.print("*");
			}
			for(Opt opt : f.getDirectives()){
				entrou = true;
				gravarArq.print(" 			" + f.getName() + "(func)" + " " + opt.getPresenceCondition() + " and ");
			}
			if(!entrou){
				gravarArq.print(" 			" + f.getName() + "(func)" + " True " + "and ");
			}
			//se não entrou, então quer dizer que a função não tem diretivas e é declarada em True
			
		}
		
		if(link.callee.id == ID.Variable){
			v = (Variable) link.callee;
			gravarArq.println("" + v.getName() +"(var)"+ " " + v.getPresenceCondition());
			
		}
		else if(link.callee.id == ID.Function){
			f = (Function) link.callee;
			boolean entrou = false;
			
			for(Opt opt : f.getDirectives()){
				entrou = true;
				gravarArq.println("" + f.getName() +"(func)" + " " + opt.getPresenceCondition());
			}
			if(!entrou){
				gravarArq.println("" + f.getName() + "(func)" + " True ");
			}
		}
		
	}
	
	public void showDependencies(List<Dependency> dps, PrintWriter gravarArq){
		for(Dependency dp : dps){
			gravarArq.println(" 		"+dp.variability1.getName() + " " +dp.variability2.getName());
		}
	}
	
	public List<Dependency> getAllNewDependencies(List<Dependency> dpsToCompare){
		List<Dependency> newDependencies = new ArrayList<Dependency>();		
		for(Dependency dp : dpsToCompare){
			if(!Metrics.allDependenciesOfAllVersions.contains(dp)){
				newDependencies.add(dp);
			}
		}
		return newDependencies;
		
	}
	
	public List<Dependency> getAllDeadDependencies(List<Dependency> dpsToCompare){
		List<Dependency> deadDependencies = new ArrayList<Dependency>();
		for(Dependency dp : Metrics.allDependenciesOfAllVersions){
			if(!dpsToCompare.contains(dp)){
				deadDependencies.add(dp);
			}
		}
		return deadDependencies;
		
	}
	
	public int[] totalNumberOfUniqueLinks(List<Link> listOfLinksFromDependency){
		int numberOfUniqueLinks = 0;
		List<Link> distinctLinks = new ArrayList<Link>();
		for(Link link : listOfLinksFromDependency){
			ProgramElement caller = link.caller;
			ProgramElement callee = link.callee;
			int numberOfEvents = 0;
			
			if(!distinctLinks.contains(link)){
				distinctLinks.add(link);
			}
			
			for(Link linkToCompare : listOfLinksFromDependency){
				if(linkToCompare.caller.equals(caller) && linkToCompare.callee.equals(callee)){
					numberOfEvents++;
				}
			}
			link.weight = numberOfEvents;
			//só entrou uma vez no if, então o link é único
			if(numberOfEvents == 1){
				link.unique = true;
				numberOfUniqueLinks++;
			}
		}
		int values[] = new int[2];
		values[0] = numberOfUniqueLinks;
		values[1] = distinctLinks.size();
		return values;
	}
	
	public void createSpreadsheetFolders(String projectFolder){
		
	}
	
	public void downloadProject(String remotePath, String version){
		System.out.println("Downloading project");
		//guardar a instancia de git faz com que o arquivo fique aberto e não é possível analisar
        try {
			Git git = Git.cloneRepository().setURI(remotePath)
			.setDirectory(new File(downloadPath)).call();
			
			final List<Ref> tagRefs;
	    	tagRefs = git.tagList().call();
	    	
	    	for(Ref ref : tagRefs) {
	            String versionNumber = ref.getName().split("/")[2];
	            String commitId = ref.getObjectId().getName();
	            download = true;
	            if(versionNumber.contains(version)){
	            	git.checkout().setCreateBranch( true ).setName( "new-branch" ).setStartPoint( commitId ).call();
	            }
	        }
			
	    	//fechar para liberar os arquivos para leitura
	    	git.getRepository().close();
			
			
			
		} catch (InvalidRemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Download done!");

	}
	
	//verificar se folder já existe
	public String createDownloadFolder(int i, String version){
		this.downloadPath = (SampleHandler.RUNTIME_WORKSPACE_PATH + "download" + i + "-" + version).replace("\\", "/"); 
		new File(downloadPath).mkdir();
		return "download" + i + "-" + version;
	}
	
	public void closeReader(BufferedReader reader){
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createProjectFolder(){
		new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT).mkdir();
		createFolderAnalysis();
	}
	
	public BufferedReader setReader(){
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(SampleHandler.RUNTIME_WORKSPACE_PATH + "/info.txt"));
			return reader;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public void createFolderAnalysis(){
		setStringToFolder();
		new File(toFolder).mkdir();
	}
	
	public void deleteFile(File f){
		try {
			FileUtils.deleteDirectory(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Deleted");
	}
	
	public void setStringToFolder(){
		toFolder = (downloadPath + "/analysis").replace("\\", "/");
	}
	
	public void moveFiles(){
		String path = downloadPath;
		try {
			filesInAnalysis(path);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
	
	public void createCProject(String name){
		IProject projectHandle = ResourcesPlugin.getWorkspace().getRoot().getProject(name);
        
		try {
			//projectHandle.create(monitor);
			//projectHandle.open(monitor);
			
			IProjectDescription description = projectHandle.getWorkspace().newProjectDescription(name);
			description.setLocationURI(projectHandle.getLocationURI() );
			IProgressMonitor monitor = new NullProgressMonitor();
			IProject project = CCorePlugin.getDefault().createCDTProject(description, projectHandle, monitor);
			IManagedBuildInfo buildInfo = ManagedBuildManager.createBuildInfo(project);
			try {
				IManagedProject projectManaged = ManagedBuildManager
				    .createManagedProject(project, 
				                          ManagedBuildManager.getExtensionProjectType("cdt.managedbuild.target.gnu.mingw.exe") );
				 
			    List<IConfiguration> configs = getValidConfigsForPlatform();
			    IConfiguration config = 
				        projectManaged.createConfiguration(
				                configs.get(0), 
				                ManagedBuildManager.calculateChildId(configs.get(0).getId(), null));
			    
			    ICProjectDescription cDescription = 
				        CoreModel.getDefault().getProjectDescriptionManager().createProjectDescription(project, false);
			    
			    ICConfigurationDescription cConfigDescription = 
				        cDescription.createConfiguration(ManagedBuildManager.CFG_DATA_PROVIDER_ID, config.getConfigurationData());
			    
			    cDescription.setActiveConfiguration(cConfigDescription);
			    cConfigDescription.setSourceEntries(null);
			    IFolder srcFolder = project.getFolder("analysis");
			    
			    ICSourceEntry srcFolderEntry = new CSourceEntry(srcFolder, null, ICSettingEntry.RESOLVED);
			    cConfigDescription.setSourceEntries(new ICSourceEntry[] { srcFolderEntry });
			
			    buildInfo.setManagedProject(projectManaged);
			
			    cDescription.setCdtProjectCreated();
			
			    IIndexManager indexMgr = CCorePlugin.getIndexManager();
			    ICProject cProject = CoreModel.getDefault().getCModel().getCProject(project.getName() );
			    indexMgr.setIndexerId(cProject, IPDOMManager.ID_FAST_INDEXER);
			
			    CoreModel.getDefault().setProjectDescription(project, cDescription);
			
			    ManagedBuildManager.setDefaultConfiguration(project, config );
			    ManagedBuildManager.setSelectedConfiguration(project, config );
			
			    ManagedBuildManager.setNewProjectVersion(project);
			
			    ManagedBuildManager.saveBuildInfo(project, true);
			} catch (BuildException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
		    
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
	}
	
	public static List<IConfiguration> getValidConfigsForPlatform() {
	    List<IConfiguration> configurations = 
	        new ArrayList<IConfiguration>();
	
	    for (IConfiguration cfg : ManagedBuildManager.getExtensionConfigurations() ) {
	        IToolChain currentToolChain =
	            cfg.getToolChain();
	
	        if ( (currentToolChain != null )                           && 
	             (ManagedBuildManager.isPlatformOk(currentToolChain) ) &&
	             (currentToolChain.isSupported() )                     ) {
	
	            configurations.add(cfg);
	        }
	    }
	    return configurations;
	}
	
	public void analyzeFilesInSrc() throws Exception{
		ICProject project = CoreModel.getDefault().getCModel().getCProject(SampleHandler.PROJECT);
		String thePath = project.getPath().toString();
		System.out.println(thePath);
		IIndex index = CCorePlugin.getIndexManager().getIndex(project);
		
		// It gets all C files from the ANALYSIS path to analyze.
		this.filesInSrc = new ArrayList<String>();
		this.setSrcFiles((SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + "/analysis").replace("\\", "/"));
		
		//resetando types e macros
		types = new ArrayList<String>();
		macros = new ArrayList<String>();
		macrosNotToInclude = new ArrayList<String>();
		
		// For each C file in the ANALYSIS folder..
		for (String file : this.filesInSrc){
			String completeFilePath = file.replace(SampleHandler.RUNTIME_WORKSPACE_PATH.replace("\\", "/"), "");
			System.out.println(completeFilePath);
			this.editDirectives(file);
			
			IPath iFilePath = new Path(completeFilePath);
			IFile iFile = ResourcesPlugin.getWorkspace().getRoot().getFile(iFilePath);
			
			ITranslationUnit tu = (ITranslationUnit) CoreModel.getDefault().create(iFile);
			
			IASTTranslationUnit ast = null;
			try {
				// We need a read-lock on the index.
				index.acquireReadLock();
			
				// The AST is ready for use..
				ast = tu.getAST(index, ITranslationUnit.AST_PARSE_INACTIVE_CODE);
				
				
				this.setTypes(ast);
				this.setMacros(ast);
				
				//System.out.println("Getting directives of file: " + new File(file).getAbsolutePath());
				this.getDirectives(new File(file));
				
				file = file.replace(SampleHandler.PROJECT + "/" + "src", SampleHandler.PROJECT + "/" + "analysis");
				this.removeDirectives(file);
				
				
			} finally {
				// Do not use the AST after release the lock.
				index.releaseReadLock();
				ast = null;          
			}
		}
	
		this.writeTypesToPlatformHeader();
		
		
		//this.removeAnalysisFiles(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "analysis");
		
	}
	
	public void setSrcFiles(String srcFolderPath){
		File[] files = new File(srcFolderPath).listFiles();
		System.out.println("files  "+srcFolderPath);
		for(File file: files){
			if(file.isDirectory()){
				setSrcFiles(file.getAbsolutePath());
		    } else {
		    	if(file.getName().trim().endsWith(".c") || file.getName().trim().endsWith(".h")){
		    		String path = file.getAbsolutePath().replace("\\", "/");		    		
					this.filesInSrc.add(path);
			    } else {
			    	file.delete();
			    }
		    }
		}
	}
	
	public void removeAnalysisFiles(String srcFolderPath){
		File[] files = new File(srcFolderPath).listFiles();
		for(File file: files){
			if(file.isDirectory()){
				removeAnalysisFiles(file.getAbsolutePath());
		    } else {
		    	if(!file.getName().trim().endsWith(".c") && !file.getName().trim().endsWith(".h")){
		    		file.delete();
			    }
		    }
		}
	}
	
	public void removeDirectives(String file) throws IOException{
		FileWriter fstreamout = new FileWriter(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "temp2.c");
		BufferedWriter out = new BufferedWriter(fstreamout);
		  
		//out.write("#include \"stubs.h\"\n");
		
		FileInputStream fstream = new FileInputStream(file);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);
  		BufferedReader br = new BufferedReader(new InputStreamReader(in));
  		String strLine;
  		//Read File Line By Line
	  	while ((strLine = br.readLine()) != null)   {
		  // Print the content on the console
	  
		  	if (strLine.contains("include") && strLine.contains("#") && ((strLine.contains("<") && strLine.contains("<")) || strLine.contains("\"")) ){
		  		out.write("//" + strLine + "\n");
		  	} else {
		  		out.write(strLine + "\n");
		  	}
		  
	  	}
	  	
	  	in.close();
	  	out.close();
	  	
	  	File original = new File(file);
	  	
	  	File temp2 = new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "temp2.c");
	  	
	  	Files.copy(temp2.toPath(),original.toPath(),StandardCopyOption.REPLACE_EXISTING);
		  
  		//new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "temp2.c").renameTo(new File(file));
		  
	}
	
	public void editDirectives(String file) throws IOException{
		FileWriter fstreamout = new FileWriter(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "temp2.c");
		BufferedWriter out = new BufferedWriter(fstreamout);
		  
		out.write("#define A\n\n");
		
		
		FileInputStream fstream = new FileInputStream(file);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);
  		BufferedReader br = new BufferedReader(new InputStreamReader(in));
  		String strLine;
  		//Read File Line By Line
  		
	  	while ((strLine = br.readLine()) != null)   {
		  // Print the content on the console
	  
		  	if (strLine.contains("#if")){
		  		out.write("\n#ifdef A\n");
		  	} else if (strLine.contains("#el")) {
		  		out.write("\n#endif\n#ifdef A\n");
		  	} else if (strLine.contains("#endif")) {
		  		out.write("\n#endif\n");
		  	} else {
		  		out.write(strLine + "\n");
		  	}
		  
	  	}
	  	
	  	in.close();
	  	out.close();
	  	
//	  	File original = new File(file);
//	  	
//	  	File temp2 = new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "temp2.c");
//	  	
//	  	Files.copy(temp2.toPath(),original.toPath(),StandardCopyOption.REPLACE_EXISTING);
	  		  	
	  	
	  	
	  //	new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "temp2.c").renameTo((new File(file)));

	}

	// It finds probable macros in the node.
	public void setMacros(IASTNode node){
		IASTPreprocessorMacroDefinition[] definitions = node.getTranslationUnit().getMacroDefinitions();
		for (IASTPreprocessorMacroDefinition definition : definitions){
			String macro = definition.getRawSignature();
			
			if (!this.macros.contains(macro)){
				this.macros.add(macro);
			}
			
		}
	}
	
	// It finds probable types in the node.
	public void setTypes(IASTNode node){
		IASTNode[] nodes = node.getChildren();
		if (node.getClass().getCanonicalName().equals("org.eclipse.cdt.internal.core.dom.parser.c.CASTTypedefNameSpecifier")){
			CASTTypedefNameSpecifier s = (CASTTypedefNameSpecifier) node;

			String type = s.getRawSignature().replace("extern", "").replace("static", "").replace("const", "").trim();
			
			if (!this.types.contains(type) && this.isValidJavaIdentifier(type)){
				this.types.add(type);
			}
		}
		for (int i = 0; i < nodes.length; i++){
			this.setTypes(nodes[i]);
		}
	}
	
	// All types found are defined in the platform.h header file.
	public void writeTypesToPlatformHeader(){
		File platform = new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "platform.h");
		try {
			platform.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "include").mkdir();
		File header = new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "include" + File.separator + "stubs.h");
		File headerPlatform = new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + File.separator + "platform.h");
		try {
			FileWriter writer = new FileWriter(header);
			FileWriter writerPlatform = new FileWriter(headerPlatform);
			String content = "";
			for (Iterator<String> i = this.types.iterator(); i.hasNext();){
				content = i.next();
				writer.write("typedef struct {} " + content + ";\n");
				writerPlatform.write("typedef struct {} " + content + ";\n");
			}
			
			for (Iterator<String> i = this.macros.iterator(); i.hasNext();){
				boolean include = true;
				
				String next = i.next();
				String strToInclue = next.trim().replaceAll("\\s+", " ");
				//System.out.println(strToInclue);
				
				for (String test : macrosNotToInclude){
					if (strToInclue.startsWith("#define " + test) || strToInclue.startsWith("# define " + test)){
						//System.out.println("DO NOT INCLUDE IT!");
						include = false; 
						break;
					}
				}
				if (include){
					writer.write(next + "\n");
					writerPlatform.write(next + "\n");
				}
			}
			
			writer.flush();
			writer.close();
			writerPlatform.flush();
			writerPlatform.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	// It returns a set with all different directives.
	public void getDirectives(File file) throws Exception{
		
		
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;

		while ((strLine = br.readLine()) != null)   {
			
			strLine = strLine.trim();
			
			if (strLine.trim().startsWith("#if") || strLine.trim().startsWith("#elif")){
				
				strLine = strLine.replaceAll("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)","");
				
				String directive = strLine.replace("#ifdef", "").replace("#ifndef", "").replace("#if", "");
				directive = directive.replace("defined", "").replace("(", "").replace(")", "");
				directive = directive.replace("||", "").replace("&&", "").replace("!", "").replace("<", "").replace(">", "").replace("=", "");
				
				String[] directivesStr = directive.split(" ");
				
				for (int i = 0; i < directivesStr.length; i++){
					if (!macrosNotToInclude.contains(directivesStr[i].trim()) && !directivesStr[i].trim().equals("") && isValidJavaIdentifier(directivesStr[i].trim())){
						macrosNotToInclude.add(directivesStr[i].trim());
					}
				}
			}
		}
		in.close();
	}
	
	public boolean isValidJavaIdentifier(String s) {
		// An empty or null string cannot be a valid identifier
	    if (s == null || s.length() == 0){
	    	return false;
	   	}

	    char[] c = s.toCharArray();
	    if (!Character.isJavaIdentifierStart(c[0])){
	    	return false;
	    }

	    for (int i = 1; i < c.length; i++){
	        if (!Character.isJavaIdentifierPart(c[i])){
	           return false;
	        }
	    }

	    return true;
	}

}
