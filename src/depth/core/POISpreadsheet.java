package depth.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tree.FunctionDef;

public class POISpreadsheet {

	Project project;
	String filename;
	Workbook workbook = new XSSFWorkbook();
	
	interface CellNames {
		public static final String TOTAL_DEPENDENCIES = "'Summary'!B1";
		public static final String TOTAL_FUNCTION_CALL_DEPENDENCIES = "'Summary'!B2";
		public static final String TOTAL_GLOBAL_VARIABLE_DEPENDENCIES = "'Summary'!B3";
		public static final String TOTAL_MANDATORY_OPTIONAL_DEPENDENCIES = "'Summary'!B4";
		public static final String TOTAL_OPTIONAL_MANDATORY_DEPENDENCIES = "'Summary'!B5";
		public static final String TOTAL_OPTIONAL_OPTIONAL_DEPENDENCIES = "'Summary'!B6";
		public static final String TOTAL_FILES = "'Summary'!B8";
		public static final String TOTAL_SUCCESSFUL_FILES = "'Summary'!B9";
		public static final String TOTAL_FILES_DIRECTIVES = "'Summary'!B10";
		public static final String TOTAL_FILES_MAINTENANCE_POINTS = "'Summary'!B11";
		public static final String TOTAL_FILES_IMPACT_POINTS = "'Summary'!B12";
		public static final String TOTAL_FILES_MAINTENANCE_OR_IMPACT_POINTS = "'Summary'!B13";
		public static final String DEPENDENCY_DIRECTIONS = "'Dependencies'!A2:A1048576";
		public static final String DEPENDENCY_TYPES = "'Dependencies'!B2:B1048576";
		public static final String TOTAL_FUNCTIONS = "'Summary'!B15";
		public static final String TOTAL_FUNCTIONS_DIRECTIVES = "'Summary'!B16";
		public static final String TOTAL_FUNCTIONS_MAINTENANCE_POINTS = "'Summary'!B17";
		public static final String TOTAL_FUNCTIONS_IMPACT_POINTS = "'Summary'!B18";
		public static final String TOTAL_FUNCTIONS_MAINTENANCE_OR_IMPACT_POINTS = "'Summary'!B19";
		public static final String TOTAL_FUNCTIONS_CALLED = "'Summary'!B20";
		public static final String MAINTENANCE_POINTS_COUNT = "'Maintenance points'!C2:C1048576";
		public static final String IMPACT_POINTS_COUNT = "'Impact points'!C2:C1048576";
		public static final String FUNCTION_CALLS = "'Functions'!C2:C1048576";
		public static final String FUNCTION_DEPENDENCIES = "'Functions'!D2:D1048576";
		public static final String FUNCTION_DIRECTIVES = "'Functions'!F2:F1048576";
	}
	
	public POISpreadsheet(Project project, String filename) {
		System.out.println("Creating spreadsheet...");
		setProject(project);
		setFilename(filename);
		
		/*System.out.println("Creating sheet Functions...");
		createSheetFunctions();
		System.out.println("Creating sheet Dependencies...");
		createSheetDependencies();
		System.out.println("Creating sheet Skipped Files...");
		createSheetSkippedFiles();
		System.out.println("Creating sheet Maintenance Points...");
		createSheetMaintenancePoints();
		System.out.println("Creating sheet Maintenance Points Details...");
		createSheetMaintenancePointsDetails();
		System.out.println("Creating sheet Impact Points...");
		createSheetImpactPoints();
		System.out.println("Creating sheet Impact Points Details...");
		createSheetImpactPointsDetails();*/
		System.out.println("Creating file Dependency Depths...");
		createSimpleFileDependencyDepths();
		//createFileDependencyDepths();
		/*System.out.println("Creating sheet Summary...");
		createSheetSummary();*/
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}
	
	public void createSheetFunctions() {
		Sheet sheet = getWorkbook().createSheet("Functions");

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "PARAMETERS", "DEPENDENCIES", "CALLS", "HAS_DIRECTIVES"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<FunctionDef, Function> functionMap : getProject().getFunctions().entrySet()) {
			row = sheet.createRow(i);
			FunctionDef functionDef = functionMap.getKey();
			Function function = functionMap.getValue();
			// Filename
			row.createCell(0).setCellValue(functionDef.getPositionFrom().getFile().substring(5));
			// Function name
			row.createCell(1).setCellValue(function.getName());
			// Total of parameters
			row.createCell(2).setCellValue(function.getParameters().size());
			// Total of dependencies
			row.createCell(3).setCellValue(function.getDependencies().size());
			// Total of calls
			row.createCell(4).setCellValue(function.getFunctionCalls().size());
			// Has directives?
			row.createCell(5).setCellValue(new Boolean(function.getDirectives().size() > 0).toString());
			i++;
			if (i > 1048575) {
				break;
			}
		}
	}
	
	public void createSheetDependencies() {
		Sheet sheet = getWorkbook().createSheet("Dependencies");

		// Inserting header
		String[] headers = {"DIRECTION", "TYPE", "PRESENCE_CONDITION", "OUTER_PRESENCE_CONDITION", "INNER_PRESENCE_CONDITION", "FUNCTION", "VARIABLE", "IS_ASSIGNMENT", "OUTER_FILENAME", "OUTER_POSITION", "INNER_FILENAME", "INNER_POSITION", "PARAMETER_ORDER", "PARAMETER"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			row.createCell(i).setCellValue(headers[i]);
		}
		
		int i = 1;
		for (Dependency dependency : getProject().getDependencies()) {
			// toString() returns all dependency metrics
			String[] data = dependency.toString().split(";");
			row = sheet.createRow(i);
			for (int j = 0; j < data.length; j++) {
				if (data[j].length() > 32766) {
					row.createCell(j).setCellValue(data[j].substring(0,32766));
				} else {
					row.createCell(j).setCellValue(data[j]);
				}
			}
			i++;
			if (i > 1048575) {
				break;
			}
		}
	}
	
	public void createSheetSkippedFiles() {
		Sheet sheet = getWorkbook().createSheet("Skipped Files");

		// Inserting header
		String[] headers = {"FILE", "REASON"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<String, String> skippedFileMap : getProject().getSkippedFiles().entrySet()) {
			row = sheet.createRow(i);
			String filename = skippedFileMap.getKey();
			String reason = skippedFileMap.getValue();
			// Filename
			row.createCell(0).setCellValue(filename);
			// Reason to be skipped
			row.createCell(1).setCellValue(reason);
			i++;
			if (i > 1048575) {
				break;
			}
		}
	}
	
	public void createSheetMaintenancePoints() {
		Sheet sheet = getWorkbook().createSheet("Maintenance Points");

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "MAINTENANCE_POINTS"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<String, Map<String, Set<String>>> maintenancePointsMap : getProject().getMaintenancePoints().entrySet()) {
			
			String filename = maintenancePointsMap.getKey();
			for (Entry<String, Set<String>> maintenancePoints : maintenancePointsMap.getValue().entrySet()) {
			
				String functionName = maintenancePoints.getKey();
				int entryPoints = maintenancePoints.getValue().size();
				
				row = sheet.createRow(i);
				// Filename
				row.createCell(0).setCellValue(filename);
				// Function name
				row.createCell(1).setCellValue(functionName);
				// Total of entry points to the function
				row.createCell(2).setCellValue(entryPoints);
				i++;
				if (i > 1048575) {
					break;
				}
			}
		}
	}
	
	public void createSheetMaintenancePointsDetails() {
		Sheet sheet = getWorkbook().createSheet("Maintenance Points Details");

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "MAINTENANCE_POINT"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<String, Map<String, Set<String>>> maintenancePointsMap : getProject().getMaintenancePoints().entrySet()) {
			
			String filename = maintenancePointsMap.getKey();
			for (Entry<String, Set<String>> maintenancePoints : maintenancePointsMap.getValue().entrySet()) {
			
				String functionName = maintenancePoints.getKey();
				for (String entryPoint : maintenancePoints.getValue()) {
					row = sheet.createRow(i);
					// Filename
					row.createCell(0).setCellValue(filename);
					// Function name
					row.createCell(1).setCellValue(functionName);
					// Entry point to the function
					row.createCell(2).setCellValue(entryPoint);
					i++;
					if (i > 1048575) {
						break;
					}
				}
			}
		}
	}
	
	public void createSheetImpactPoints() {
		Sheet sheet = getWorkbook().createSheet("Impact Points");

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "IMPACT_POINTS"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<String, Map<String, Set<String>>> impactPointsMap : getProject().getImpactPoints().entrySet()) {
			
			String filename = impactPointsMap.getKey();
			for (Entry<String, Set<String>> impactPoints : impactPointsMap.getValue().entrySet()) {
			
				String functionName = impactPoints.getKey();
				int references = impactPoints.getValue().size();
				
				row = sheet.createRow(i);
				// Filename
				row.createCell(0).setCellValue(filename);
				// Function name
				row.createCell(1).setCellValue(functionName);
				// Total of impact points in the function
				row.createCell(2).setCellValue(references);
				i++;
				if (i > 1048575) {
					break;
				}
			}
		}
	}
	
	public void createSheetImpactPointsDetails() {
		Sheet sheet = getWorkbook().createSheet("Impact Points Details");

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "IMPACT_POINT"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<String, Map<String, Set<String>>> impactPointsMap : getProject().getImpactPoints().entrySet()) {
			
			String filename = impactPointsMap.getKey();
			for (Entry<String, Set<String>> impactPoints : impactPointsMap.getValue().entrySet()) {
			
				String functionName = impactPoints.getKey();
				for (String impactPoint : impactPoints.getValue()) {
					row = sheet.createRow(i);
					// Filename
					row.createCell(0).setCellValue(filename);
					// Function name
					row.createCell(1).setCellValue(functionName);
					// Impact in the function
					row.createCell(2).setCellValue(impactPoint);
					i++;
					if (i > 1048575) {
						break;
					}
				}
			}
		}
	}
	
	private void createSheetDependencyDepths() {
		Sheet sheet = getWorkbook().createSheet("Dependency Depths");

		// Inserting header
		String[] headers = {"CALLEE_FUNCTION", "FILE", "CALLER_FUNCTION", "FILE", "DEPTH", "CALL SEQUENCE"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<Dependency, List<List<GraphNode>>> dependencyPaths : getProject().getDependencyPaths().entrySet()) {
			FunctionCallDependency dependency = (FunctionCallDependency) dependencyPaths.getKey();
			List<List<GraphNode>> paths = dependencyPaths.getValue();
			/*List<GraphNode> maxPath = new ArrayList<GraphNode>();
			int maxSize = -1;
			for (List<GraphNode> path : paths) {
				if (path.size() > maxSize) {
					maxSize = path.size();
					maxPath = path;
				}
			}*/
			for (List<GraphNode> maxPath : paths) {
				Function callee = dependency.getCallee();
				String calleeFile = callee.getFunctionDef().getPositionFrom().getFile();
				
				FunctionDef lastCaller = maxPath.get(maxPath.size() - 1).getFunctionDef();
				String callerFile = lastCaller.getPositionFrom().getFile();
				int depth = maxPath.size() - 1;
				
				StringBuilder sequence = new StringBuilder();
				for (GraphNode graphNode : maxPath) {
					sequence.append(graphNode.getFunctionName() + " < ");
				}
				sequence.setLength(sequence.length() - 3);
				
				row = sheet.createRow(i);
				// Callee function name
				row.createCell(0).setCellValue(callee.getName());
				// Callee file
				row.createCell(1).setCellValue(calleeFile);
				// Caller function name
				row.createCell(2).setCellValue(project.getFunctions().get(lastCaller).getName());
				// Caller file
				row.createCell(3).setCellValue(callerFile);
				// Depth (steps from caller to callee)
				row.createCell(4).setCellValue(depth);
				// Sequence (all the linking through the functions)
				row.createCell(5).setCellValue(sequence.toString());
				i++;
				if (i > 1048575) {
					break;
				}
			}
		}
		
	}
	
	private void createSimpleFileDependencyDepths() {
		File file = new File(this.filename + ".csv");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileWriter fw = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(fw);
		
		// Inserting header
		String headers = "DEPTH";
		try {
			bw.write(headers);
			bw.newLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<List<GraphNode>> paths = new ArrayList<List<GraphNode>>();
		FunctionCallDependency dependency = null;
		int depth;
		for (Entry<Dependency, List<List<GraphNode>>> dependencyPaths : getProject().getDependencyPaths().entrySet()) {
			dependency = (FunctionCallDependency) dependencyPaths.getKey();
			paths = dependencyPaths.getValue();
			for (List<GraphNode> maxPath : paths) {
				depth = maxPath.size() - 1;
				try {
					bw.write(String.valueOf(depth));
					bw.newLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void createFileDependencyDepths() {
		File file = new File(this.filename + ".csv");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileWriter fw = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(fw);
		
		// Inserting header
		String headers = "#;CALLEE_FUNCTION;FILE;CALLER_FUNCTION;FILE;DEPTH;CALL SEQUENCE";
		try {
			bw.write(headers);
			bw.newLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int i = 1;
		List<List<GraphNode>> paths = new ArrayList<List<GraphNode>>();
		FunctionCallDependency dependency = null;
		Function callee = null;
		String calleeFile = null;
		FunctionDef lastCaller = null;
		String callerFile = null;
		int depth;
		StringBuilder sequence = null;
		for (Entry<Dependency, List<List<GraphNode>>> dependencyPaths : getProject().getDependencyPaths().entrySet()) {
			dependency = (FunctionCallDependency) dependencyPaths.getKey();
			paths = dependencyPaths.getValue();
			/*List<GraphNode> maxPath = new ArrayList<GraphNode>();
			int maxSize = -1;
			for (List<GraphNode> path : paths) {
				if (path.size() > maxSize) {
					maxSize = path.size();
					maxPath = path;
				}
			}*/
			for (List<GraphNode> maxPath : paths) {
				callee = dependency.getCallee();
				calleeFile = callee.getFunctionDef().getPositionFrom().getFile();
				
				lastCaller = maxPath.get(maxPath.size() - 1).getFunctionDef();
				callerFile = lastCaller.getPositionFrom().getFile();
				depth = maxPath.size() - 1;
				
				sequence = new StringBuilder();
				for (GraphNode graphNode : maxPath) {
					sequence.append(graphNode.getFunctionName() + " < ");
				}
				sequence.setLength(sequence.length() - 3);
				
				try {
					bw.write(i + ";" + callee.getName() + ";" + calleeFile + ";" + project.getFunctions().get(lastCaller).getName() + ";" + callerFile + ";" + depth + ";" + sequence.toString());
					bw.newLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i++;
			}
		}
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createSheetSummary() {
		Cell cell;
		Sheet sheet = getWorkbook().createSheet("Summary");
		CellStyle percentageStyle = getWorkbook().createCellStyle();
		percentageStyle.setDataFormat(getWorkbook().createDataFormat().getFormat("0.000%"));
		
		Set<String> filesWithDirectives = new HashSet<String>();
		Set<String> filesWithMaintenancePoints = new HashSet<String>();
		Set<String> filesWithImpactPoints = new HashSet<String>();
		Set<String> filesWithMaintenanceOrImpactPoints = new HashSet<String>();
		Set<String> functionsWithMaintenanceOrImpactPoints = new HashSet<String>();
		
		// Get all files with maintenance/impact points
		for (Dependency dependency : getProject().getDependencies()) {
			// Only Function Call Dependencies features a caller function (maintenance point)
			if (dependency instanceof FunctionCallDependency) {
				filesWithMaintenancePoints.add(dependency.getOuterFile());
			}
			filesWithImpactPoints.add(dependency.getInnerFile());
		}
		
		filesWithMaintenanceOrImpactPoints.addAll(filesWithMaintenancePoints);
		filesWithMaintenanceOrImpactPoints.addAll(filesWithImpactPoints);
		
		// Get all files with directives
		for (Entry<FunctionDef, Function> functionMap : getProject().getFunctions().entrySet()) {
			Function function = functionMap.getValue();
			if (!(function.getDirectives().isEmpty())) {
				filesWithDirectives.add(function.getFunctionDef().getPositionFrom().getFile());
			}
		}
		
		// Get all functions containing maintenance points
		for (Entry<String, Map<String, Set<String>>> maintenancePointsMap : getProject().getMaintenancePoints().entrySet()) {	
			for (Entry<String, Set<String>> maintenancePoints : maintenancePointsMap.getValue().entrySet()) {
				functionsWithMaintenanceOrImpactPoints.add(maintenancePointsMap.getKey() + ":" + maintenancePoints.getKey());
			}
		}
		
		// Get all functions containing impact points
		for (Entry<String, Map<String, Set<String>>> impactPointsMap : getProject().getImpactPoints().entrySet()) {	
			for (Entry<String, Set<String>> impactPoints : impactPointsMap.getValue().entrySet()) {
				functionsWithMaintenanceOrImpactPoints.add(impactPointsMap.getKey() + ":" + impactPoints.getKey());
			}
		}
		
		int i = 0;
		
		Row row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of dependencies");
		row.createCell(1).setCellValue(getProject().getDependencies().size());
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Function call dependencies");
		// Count occurrences of FunctionCallDependency
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_TYPES + ", \"FunctionCallDependency\")");
		cell = row.createCell(2);
		// Percentage of function call dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_FUNCTION_CALL_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Global variable dependencies");
		// Count occurrences of GlobalVariableDependency
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_TYPES + ", \"GlobalVariableDependency\")");
		cell = row.createCell(2);
		// Percentage of function call dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_GLOBAL_VARIABLE_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Mandatory<->Optional");
		// Count occurrences of Mandatory<->Optional
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_DIRECTIONS + ", \"Mandatory<->Optional\")");
		cell = row.createCell(2);
		// Percentage of Mandatory<->Optional dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_MANDATORY_OPTIONAL_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Optional<->Mandatory");
		// Count occurrences of Optional<->Mandatory
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_DIRECTIONS + ", \"Optional<->Mandatory\")");
		cell = row.createCell(2);
		// Percentage of Optional<->Mandatory dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_OPTIONAL_MANDATORY_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Optional<->Optional");
		// Count occurrences of Optional<->Optional
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_DIRECTIONS + ", \"Optional<->Optional\")");
		cell = row.createCell(2);
		// Percentage of Optional<->Optional dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_OPTIONAL_OPTIONAL_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		i++;		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of files");
		row.createCell(1).setCellValue(getProject().getTotalFiles());
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of successfully analyzed files");
		row.createCell(1).setCellValue(getProject().getTotalSuccessfulFiles());
		cell = row.createCell(2);
		// Percentage of successful files out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_FILES + "=0, 0, " + CellNames.TOTAL_SUCCESSFUL_FILES + "/" + CellNames.TOTAL_FILES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of files with directives");
		row.createCell(1).setCellValue(filesWithDirectives.size());
		cell = row.createCell(2);
		// Percentage of files with directives out of total successful files
		cell.setCellFormula("IF(" + CellNames.TOTAL_SUCCESSFUL_FILES + "=0, 0, " + CellNames.TOTAL_FILES_DIRECTIVES + "/" + CellNames.TOTAL_SUCCESSFUL_FILES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of files with maintenance points");
		row.createCell(1).setCellValue(filesWithMaintenancePoints.size());
		cell = row.createCell(2);
		// Percentage of files with maintenances points out of total files with directives
		cell.setCellFormula("IF(" + CellNames.TOTAL_SUCCESSFUL_FILES + "=0, 0, " + CellNames.TOTAL_FILES_MAINTENANCE_POINTS + "/" + CellNames.TOTAL_SUCCESSFUL_FILES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of files with impact points");
		row.createCell(1).setCellValue(filesWithImpactPoints.size());
		cell = row.createCell(2);
		// Percentage of files with impact points out of total files with directives
		cell.setCellFormula("IF(" + CellNames.TOTAL_SUCCESSFUL_FILES + "=0, 0, " + CellNames.TOTAL_FILES_IMPACT_POINTS + "/" + CellNames.TOTAL_SUCCESSFUL_FILES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of files with maintenance or impact points");
		row.createCell(1).setCellValue(filesWithMaintenanceOrImpactPoints.size());
		cell = row.createCell(2);
		// Percentage of files with maintenances or impact points out of total files with directives
		cell.setCellFormula("IF(" + CellNames.TOTAL_SUCCESSFUL_FILES + "=0, 0, " + CellNames.TOTAL_FILES_MAINTENANCE_OR_IMPACT_POINTS + "/" + CellNames.TOTAL_SUCCESSFUL_FILES + ")");
		cell.setCellStyle(percentageStyle);
		
		i++;
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions");
		row.createCell(1).setCellValue(getProject().getFunctions().size());
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions with directives");
		// Count all functions with directives
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.FUNCTION_DIRECTIVES + ", \"true\")");
		cell = row.createCell(2);
		// Percentage of functions with directives out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_DIRECTIVES + "/" + CellNames.TOTAL_FUNCTIONS + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions with maintenance points");
		// Count all functions with maintenance points
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.MAINTENANCE_POINTS_COUNT + ", \">0\")");
		cell = row.createCell(2);
		// Percentage of files with maintenances points out of total files with directives
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_MAINTENANCE_POINTS + "/" + CellNames.TOTAL_FUNCTIONS + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions with impact points");
		// Count all functions with impact points
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.IMPACT_POINTS_COUNT + ", \">0\")");
		cell = row.createCell(2);
		// Percentage of files with impact points out of total files with directives
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_IMPACT_POINTS + "/" + CellNames.TOTAL_FUNCTIONS + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions with maintenance or impact points");
		row.createCell(1).setCellValue(functionsWithMaintenanceOrImpactPoints.size());
		cell = row.createCell(2);
		// Percentage of functions with maintenances or impact points out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_MAINTENANCE_OR_IMPACT_POINTS + "/" + CellNames.TOTAL_FUNCTIONS + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions called");
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.FUNCTION_CALLS + ", \">0\")");;
		cell = row.createCell(2);
		// Percentage of functions called out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_CALLED + "/" + CellNames.TOTAL_FUNCTIONS + ")");
		cell.setCellStyle(percentageStyle);
		
		// Statistical metrics per function
		i++;
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Metric");
		row.createCell(1).setCellValue("Mean");
		row.createCell(2).setCellValue("Median");
		row.createCell(3).setCellValue("Mode");
		row.createCell(4).setCellValue("Std. deviation");
		row.createCell(5).setCellValue("Minimum");
		row.createCell(6).setCellValue("Maximum");
		row.createCell(7).setCellValue("Sum");
		// Maintenance points
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Maintenance points");
		row.createCell(1).setCellFormula("AVERAGE(" + CellNames.MAINTENANCE_POINTS_COUNT + ")");
		row.createCell(2).setCellFormula("MEDIAN(" + CellNames.MAINTENANCE_POINTS_COUNT + ")");
		row.createCell(3).setCellFormula("MODE(" + CellNames.MAINTENANCE_POINTS_COUNT + ")");
		row.createCell(4).setCellFormula("STDEV(" + CellNames.MAINTENANCE_POINTS_COUNT + ")");
		row.createCell(5).setCellFormula("MIN(" + CellNames.MAINTENANCE_POINTS_COUNT + ")");
		row.createCell(6).setCellFormula("MAX(" + CellNames.MAINTENANCE_POINTS_COUNT + ")");
		row.createCell(7).setCellFormula("SUM(" + CellNames.MAINTENANCE_POINTS_COUNT + ")");
		// Impact points
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Impact points");
		row.createCell(1).setCellFormula("AVERAGE(" + CellNames.IMPACT_POINTS_COUNT + ")");
		row.createCell(2).setCellFormula("MEDIAN(" + CellNames.IMPACT_POINTS_COUNT + ")");
		row.createCell(3).setCellFormula("MODE(" + CellNames.IMPACT_POINTS_COUNT + ")");
		row.createCell(4).setCellFormula("STDEV(" + CellNames.IMPACT_POINTS_COUNT + ")");
		row.createCell(5).setCellFormula("MIN(" + CellNames.IMPACT_POINTS_COUNT + ")");
		row.createCell(6).setCellFormula("MAX(" + CellNames.IMPACT_POINTS_COUNT + ")");
		row.createCell(7).setCellFormula("SUM(" + CellNames.IMPACT_POINTS_COUNT + ")");
	}

	public void write() {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(getFilename()));
			getWorkbook().write(fos);
			 
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error on writing to file " + getFilename());
		} finally {
			try {
				fos.flush();
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Spreadsheet created!");
	}
	
}
