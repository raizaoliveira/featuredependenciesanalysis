package depth.core;

import tree.FunctionDef;

public class GraphEdgeHelper {

	private GraphNode graphNode;
	private int parameterPosition;
	private FunctionDef functionDef;
	
	public GraphNode getGraphNode() {
		return graphNode;
	}

	public void setGraphNode(GraphNode graphNode) {
		this.graphNode = graphNode;
	}

	public int getParameterPosition() {
		return parameterPosition;
	}

	public void setParameterPosition(int parameterPosition) {
		this.parameterPosition = parameterPosition;
	}

	public FunctionDef getFunctionDef() {
		return functionDef;
	}

	public void setFunctionDef(FunctionDef functionDef) {
		this.functionDef = functionDef;
	}

	public GraphEdgeHelper(GraphNode graphNode, int parameterPosition,
			FunctionDef functionDef) {
		this.graphNode = graphNode;
		this.parameterPosition = parameterPosition;
		this.functionDef = functionDef;
	}
}
