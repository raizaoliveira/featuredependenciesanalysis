package depth.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Test {

	public static void main(String[] args) {
		long startTime = System.nanoTime();
		List<String> folders = new ArrayList<String>() {{
//			add("atlantisos");
//			add("bash2.01");
//			add("bc1.03");
//			add("berkeley");
//			add("bison2.0");
//			add("apache");
//			add("cherokee");
//			add("clamav");
//			add("gnuplot");
//			add("gzip1.2.4");
//			add("libssh0.5.3");
//			add("libxml2");
//			add("lighttpd");
			add("lua");
//			add("mpsolve");
//			add("sendmail"); 
//			add("sqlite");
//			add("vim7.3");
//			add("xfig3.2.3");
//			add("xterm2.9.1");
//			//add("test");
//			add("cvs-1.11.21");
//			add("dia");
//			add("expat2.1.0");
//			add("flex2.5.35");
//			add("fvwm2.4.15");
//			add("gawk3.1.4");
//			add("gnuchess5.06");
//			add("irssi");
//			add("kindb");
//			add("libdsmcc0.6");
//			add("libieee0.2.11");
//			add("libsoup");
//			add("lynx");
//			add("m4-1.4.4");
//			add("mptris-1.9");
//			add("prc-tools");
//			add("rcs5.7");
//			add("sylpheed");
//			add("privoxy");
//			add("libpng1.0.60");
		}};
		for (String folder : folders) {
			Project project;
			File platform = new File("interprocedural\\" + folder + "\\platform.h");
			if (platform.length() == 0) {
				project = new Project("interprocedural\\" + folder + "\\analysis\\", "interprocedural\\" + folder + "\\include\\stubs.h");
			} else {
				project = new Project("interprocedural\\" + folder + "\\analysis\\", "interprocedural\\" + folder + "\\platform.h");
			}
			project.analyze();
			project.generateMetrics();
			POISpreadsheet m = new POISpreadsheet(project, "C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\depth\\" + folder + ".xlsx");
			m.write();
		}
		long elapsedTime = System.nanoTime() - startTime;
		System.out.println("Analysis ended in " + TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS) + " seconds.");
	}

}
