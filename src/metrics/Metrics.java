package metrics;

import java.util.ArrayList;
import java.util.List;

import analysis.Dependency;
import analysis.ID;
import analysis.Link;
import analysis.core.Function;
import tree.Opt;

public class Metrics {
	
	public static int numberOfVariabilities;
	public static int numberOfDependencies;
	
	public static List<List<Link>> allLinksFromDependenciesOfAllVersions;
	//Lista de lista, onde cada lista dentro da lista corresponde as dependencias e links dessas dependencias da vers�o correspondente
	public static List<List<Dependency>> allDependenciesAndLinksDefined;
	//Lista de dependencias do software inteiro de todas as vers�es
	public static List<Dependency> allDependenciesOfAllVersions;
	
	
	
	public static List<String> getDependenciesToShow(List<Link> dps){
		List<String> returnList = new ArrayList<String>();
		for(Link link : dps){
			String toBeAdded = "";
			//verificando o caller
			if(link.caller.id == ID.Variable){
				toBeAdded = toBeAdded + link.caller.getName() + " " + link.caller.getPresenceCondition() + " ";
			}
			else if(link.caller.id == ID.Function){
				Function f = (Function) link.caller;
				boolean entrou = false;
				for(Opt opt : f.getDirectives()){
					entrou = true;
					toBeAdded = toBeAdded + f.getName() + " " + opt.getPresenceCondition() + " ";
				}
				if(!entrou){
					toBeAdded = toBeAdded + f.getName() + " True ";
				}
			}
			
			//verificano o callee
			if(link.callee.id == ID.Variable){
				toBeAdded = toBeAdded + link.callee.getName() + " " +link.callee.getPresenceCondition() + " ";
			}
			else if(link.callee.id == ID.Function){
				Function f = (Function) link.callee;
				for(Opt opt : f.getDirectives()){
					toBeAdded = toBeAdded + f.getName() + " " + opt.getPresenceCondition() + " ";
				}
			}
			returnList.add(toBeAdded);
		}
		return returnList;
	}
	
}
