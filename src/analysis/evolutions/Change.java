package analysis.evolutions;

import java.util.ArrayList;
import java.util.List;


public class Change {

	ArrayList<String> type = new ArrayList<>();
	ArrayList<String> modifier = new ArrayList<>();
	ArrayList<String> qualifier = new ArrayList<>();
	ArrayList<String> specifier = new ArrayList<>();
	ArrayList<String> n_parameters = new ArrayList<>();
	
	private String idDependency; 
	public String dependency;
	private int variable_changes;
	private int function_changes;
	private int type_changes;
	private int modifier_changes;
	private int qualifier_changes;
	private int specifier_changes;
	private int parameters_changes;
	private int parameters_qtd_changes;
	private int parameters_type_changes;
	private int parameters_modifier_changes;
	private int parameters_qualifier_changes;
	
	public Change() {
		// TODO Auto-generated constructor stub
	}
	
	public void set_type(String type) {
		this.type.add(type);
	}
	
	public void set_modifier(String modifier) {
		this.modifier.add(modifier);
	}
	
	public void set_qualifier(String qualifier) {
		this.qualifier.add(qualifier);
	}
	
	public void set_specifier(String specifier) {
		this.specifier.add(specifier);
	}
	
	
	public ArrayList<String> get_type() {
		return type;
	}
	
	public ArrayList<String> get_modifier() {
		return modifier;
	}
	
	public ArrayList<String> get_qualifier() {
		return qualifier;
	}
	
	public ArrayList<String> get_specifier() {
		return specifier;
	}
	
	public int getVariable_changes() {
		return variable_changes;
	}
	public void setVariable_changes(int variable_changes) {
		this.variable_changes = variable_changes;
	}
	public int getFunction_changes() {
		return function_changes;
	}
	public void setFunction_changes(int function_changes) {
		this.function_changes = function_changes;
	}
	public int getType_changes() {
		return type_changes;
	}
	public void setType_changes(int type_changes) {
		this.type_changes = type_changes;
	}
	public int getModifier_changes() {
		return modifier_changes;
	}
	public void setModifier_changes(int modifier_changes) {
		this.modifier_changes = modifier_changes;
	}
	public int getQualifier_changes() {
		return qualifier_changes;
	}
	public void setQualifier_changes(int qualifier_changes) {
		this.qualifier_changes = qualifier_changes;
	}
	public int getSpecifier_changes() {
		return specifier_changes;
	}
	public void setSpecifier_changes(int specifier_changes) {
		this.specifier_changes = specifier_changes;
	}
	public int getParameters_changes() {
		return parameters_changes;
	}
	public void setParameters_changes(int parameters_changes) {
		this.parameters_changes = parameters_changes;
	}
	public int getParameters_qtd_changes() {
		return parameters_qtd_changes;
	}
	public void setParameters_qtd_changes(int parameters_qtd_changes) {
		this.parameters_qtd_changes = parameters_qtd_changes;
	}
	public int getParameters_type_changes() {
		return parameters_type_changes;
	}
	public void setParameters_type_changes(int parameters_type_changes) {
		this.parameters_type_changes = parameters_type_changes;
	}
	public int getParameters_modifier_changes() {
		return parameters_modifier_changes;
	}
	public void setParameters_modifier_changes(int parameters_modifier_changes) {
		this.parameters_modifier_changes = parameters_modifier_changes;
	}
	public int getParameters_qualifier_changes() {
		return parameters_qualifier_changes;
	}
	public void setParameters_qualifier_changes(int parameters_qualifier_changes) {
		this.parameters_qualifier_changes = parameters_qualifier_changes;
	}
	public String getDependency() {
		return dependency;
	}
	public void setDependency(String dependency) {
		this.dependency = dependency;
	}

	public String getIdDependency() {
		return idDependency;
	}

	public void setIdDependency(String idDependency) {
		this.idDependency = idDependency;
	}

	

}
