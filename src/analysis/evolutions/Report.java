package analysis.evolutions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.util.SystemOutLogger;

import analysis.core.Function;
import analysis.core.ProgramElement;
import analysis.core.Project;
import tree.Opt;
import tree.Variable;
import analysis.ID;
import analysis.Link;

public class Report{
	List<Change> changes = new ArrayList<>();
	Change change;
	private int variable_changes = 0;
	private int function_changes = 0;
	private int type_changes = 0;
	private int return_type = 0;
	private int modifier_changes = 0;
	private int qualifier_changes = 0;
	private int specifier_changes = 0;
	private int parameters_changes = 0;
	private int parameters_qtd_changes = 0;
	private int parameters_type_changes = 0;
	private int parameters_modifier_changes = 0;
	private int parameters_qualifier_changes = 0;
	


	public Report(Project v0, Project v1){
		int i;
		String id;
		String var1 = null, var2 = null;
		//para cada dependencia comparar todos os links
		
		System.out.println("Report");
		System.out.println(v0.getdependencies().size());
		System.out.println(v1.getdependencies().size());
		
		
		for(i = 0; i < v0.getdependencies().size(); i++) {
			for(int j = 0; j < v1.getdependencies().size(); j++) {
				
				if (v0.getdependencies().get(i).get_callee().id == ID.Function) {
					if((((Function)v0.getdependencies().get(i).get_caller()).getDirectives()).size() == 0) {
						var1 = "True";	
					}else {
						for(Opt opt: ((Function)v0.getdependencies().get(i).get_caller()).getDirectives()) {
							var1 = opt.getPresenceCondition().toString();
						}
					}	
				}
				
				if (v1.getdependencies().get(j).get_callee().id == ID.Function) {
					if (((Function)v1.getdependencies().get(j).get_callee()).getDirectives().size() == 0) {
						var2 = "True";
					}else {
						for(Opt opt: ((Function)v1.getdependencies().get(j).get_callee()).getDirectives()) {
							var2 = opt.getPresenceCondition().toString();
				
						}
					}
				}
				
				if (v0.getdependencies().get(i).get_callee().id == ID.Variable) {
					if((v0.getdependencies().get(i).get_callee()).getPresenceCondition().size() == 0) {
						var1 = "True";	
					}else {
						var1 = v0.getdependencies().get(i).get_callee().getPresenceCondition().toString();
						
					}	
				}
				
				if (v1.getdependencies().get(j).get_callee().id == ID.Variable) {
					if((v1.getdependencies().get(j).get_callee()).getPresenceCondition().size() == 0) {
						var2 = "True";	
					}else {
						var2 = v1.getdependencies().get(j).get_callee().getPresenceCondition().toString();
						
					}	
				}
				
				if (v0.getdependencies().get(i).get_caller().id == ID.Variable) {
					if((v0.getdependencies().get(i).get_caller()).getPresenceCondition().size() == 0) {
						var1 = "True";	
					}else {
						var1 = v0.getdependencies().get(i).get_caller().getPresenceCondition().toString();
						
					}	
				}
				
				if (v1.getdependencies().get(j).get_caller().id == ID.Variable) {
					if((v1.getdependencies().get(j).get_caller()).getPresenceCondition().size() == 0) {
						var2 = "True";	
					}else {
						var2 = v1.getdependencies().get(j).get_caller().getPresenceCondition().toString();
						
					}	
				}
				
				
				
				System.out.println("ID: "+var1+"  "+var2);
				change = new Change();
				change.setIdDependency(var1+"  "+var2);
		
				if(differ(v0.getdependencies().get(i), v1.getdependencies().get(j))) {
					//System.out.println(((Function)v0.getdependencies().get(i).get_caller()).getName()+" = "+((Function)v1.getdependencies().get(j).get_caller()).getName()+" , "+((Function)v0.getdependencies().get(i).get_callee()).getName()+" = "+((Function)v1.getdependencies().get(i).get_callee()).getName());
					id = ( v0.getdependencies().get(i).get_caller().getName())+"  "+( v1.getdependencies().get(j).get_callee().getName());
					System.out.println("aloooo");
					change.setDependency(id);
					change.setFunction_changes(function_changes);
					change.setVariable_changes(variable_changes);
					change.setModifier_changes(modifier_changes);
					change.setQualifier_changes(qualifier_changes);
					change.setSpecifier_changes(specifier_changes);
					change.setType_changes(type_changes);
					change.setParameters_modifier_changes(parameters_modifier_changes);
					change.setParameters_qualifier_changes(parameters_qualifier_changes);
					change.setParameters_type_changes(parameters_type_changes);
					change.setParameters_qtd_changes(parameters_qtd_changes);
					changes.add(change);						
				}	
			}					
		}
		GeneratorPDF pdf = new GeneratorPDF(changes);	
	}	
		

	public Boolean differ(Link a, Link b){
		Boolean flag = false;
		if (a.get_caller().id == ID.Function) {
			if (compare_parameters(a, b)) {
				flag = true;
			}
		}
		if (compare_type(a.get_caller(), b.get_caller(), 0)) {
			flag = true;
		}
		if (compare_modifier(a.get_caller(), b.get_caller(), 0)) {
			flag = true;
			modifier_changes++;
		}
		if (compare_qualifier(a.get_caller(), b.get_caller(), 0)) {
			flag = true;
			qualifier_changes++;
			}
		if (compare_specifier(a.get_caller(), b.get_caller(), 0)) {
			flag = true;
			specifier_changes++;
			}
		
		
		if (a.get_callee().id == ID.Function) {
			compare_parameters(a, b);
		}
		if (compare_type(a.get_callee(), b.get_callee(), 1)) {
			flag = true;
		}
		if (compare_modifier(a.get_callee(), b.get_callee(), 1)) {
			flag = true;
			modifier_changes++;
		}		
		if (compare_qualifier(a.get_callee(), b.get_callee(), 1)) {
			flag = true;
			qualifier_changes++;
		}
		if (compare_specifier(a.get_callee(), b.get_callee(), 1)) {
			flag = true;
			specifier_changes++;
			}
		return flag;

	}
	
	
	public Boolean compare_type(ProgramElement a, ProgramElement  b, int element) {
		Boolean flag = false;
		if(a.id == ID.Function) {
			if (a.getType() != b.getType()) {
				function_changes++;
				return_type++;
				flag = true;
				if (element == 0) {//caller
					change.type.add("("+a.getName() +") Caller Return Type: "+a.getType()+" <> "+ b.getType());
				}else if (element == 1) {//callee
					change.type.add("("+a.getName() +") Callee Return Type: "+a.getType()+" <> "+ b.getType());
				}
			}			
		}else if (a.id == ID.Variable) {
			if (a.getType() != b.getType()) {
				variable_changes++;
				type_changes++;
				flag = true;
				if (element == 0) {//caller
					change.type.add("("+a.getName() +") Caller Type: "+a.getType()+" <> "+ b.getType());
				}else if (element == 1) {//callee
					change.type.add("("+a.getName() +") Callee Type: "+a.getType()+" <> "+ b.getType());
				}
			}
		}else if (a.id == null ) {
				if (a.getType() != b.getType()) {
					flag = true;
					System.out.println(a.getType()+" != "+b.getType());
					parameters_type_changes++;
					System.out.println("p changes "+parameters_type_changes);
					change.type.add("("+a.getName() +") Parameter Type: "+a.getType()+" <> "+ b.getType());	
				}
		}
		return flag;
	}
	public Boolean compare_modifier(ProgramElement a, ProgramElement  b, int element) {
		Boolean flag = false;
		if(a.id == ID.Function) {
			if (a.getModifier() != b.getModifier()) {
				function_changes++;
				flag = true;
				if (element == 0) {//caller
					change.modifier.add("Caller Return Modifier: "+a.getModifier()+" <> "+ b.getModifier());
				}else if (element == 1) {//callee
					change.modifier.add("Callee Return Modifier: "+a.getModifier()+" <> "+ b.getModifier());
				}
			}			
		}else if (a.id == ID.Variable) {
			if (a.getModifier() != b.getModifier()) {
				variable_changes++;
				flag = true;
				if (element == 0) {//caller
					change.modifier.add("Caller Modifier: "+a.getModifier()+" <> "+ b.getModifier());
				}else if (element == 1) {//callee
					change.modifier.add("Callee Modifier: "+a.getModifier()+" <> "+ b.getModifier());
				}
			}
		}else if (a.id == null) {
			if (a.getModifier() != b.getModifier()) {
				flag = true;
				parameters_modifier_changes++;
				change.modifier.add("Parameter Modifier: "+a.getModifier()+" <> "+ b.getModifier());
			}
		}
		return flag;
	}
	public Boolean compare_qualifier(ProgramElement a, ProgramElement  b, int element) {
		Boolean flag = false;
		if(a.id == ID.Function) {
			if (a.getQualificator() != b.getQualificator()) {
				System.out.println("MUDAN�A QUALIFICADOR  "+((Function)a).getName());
				function_changes++;
				flag = true;
				if (element == 0) {//caller
					change.qualifier.add("("+a.getName() +") Caller Return Qualifier: "+a.getQualificator()+" <> "+ b.getQualificator());
				}else if (element == 1) {//callee
					change.qualifier.add("("+a.getName() +") Callee Return Qualifier: "+a.getQualificator()+" <> "+ b.getQualificator());
				}
			}			
		}else if (a.id == ID.Variable) {
			if (a.getQualificator() != b.getQualificator()) {
				variable_changes++;
				flag = true;
				if (element == 0) {//caller
					change.qualifier.add("("+a.getName() +") Caller Qualifier: "+a.getQualificator()+" <> "+ b.getQualificator());
				}else if (element == 1) {//callee
					change.qualifier.add("("+a.getName() +") Callee Qualifier: "+a.getQualificator()+" <> "+ b.getQualificator());
				}
			}
		}else if (a.id == null) {
			if (a.getQualificator() != b.getQualificator()) {
				flag = true;
				parameters_qualifier_changes++;
				change.qualifier.add("("+a.getName() +") Parameter Qualifier: "+a.getQualificator()+" <> "+ b.getQualificator());	
			}
			
		}
		return flag;
		
	}
	public Boolean compare_specifier(ProgramElement a, ProgramElement  b, int element) {
		Boolean flag = false;
		if(a.id == ID.Function) {
			if (a.getSpecifier()!= b.getSpecifier()) {
				function_changes++;
				flag = true;
				if (element == 0) {//caller
					change.specifier.add("Caller Return Specifier: "+a.getSpecifier()+" <> "+ b.getSpecifier());
				}else if (element == 1) {//callee
					change.specifier.add("Callee Return Specifier: "+a.getSpecifier()+" <> "+ b.getSpecifier());
				}
			}			
		}else if (a.id == ID.Variable) {
			if (a.getSpecifier() != b.getSpecifier()) {
				variable_changes++;
				flag = true;
				if (element == 0) {//caller
					change.specifier.add("Caller Specifier: "+a.getSpecifier()+" <> "+ b.getSpecifier());
				}else if (element == 1) {//callee
					change.specifier.add("Callee Specifier: "+a.getSpecifier()+" <> "+ b.getSpecifier());
				}
			}
		}
		return flag;
	}
	
	
	public Boolean compare_parameters(Link a, Link b){
		Boolean flag = false;
		if ((((Function)a.get_caller()).getParameters()).size() != (((Function)b.get_caller()).getParameters()).size()) {
			parameters_qtd_changes++;
			flag = true;
		
		}
		for(Variable var: ((Function)a.get_caller()).getParameters()) {
			for(Variable v: ((Function)b.get_caller()).getParameters()) {
				if (var.getName().compareTo(v.getName()) == 0) {
					if (compare_type(var, v, 2 )) {flag = true;}
					if (compare_modifier(var, v, 2)) {flag = true;}
					if (compare_qualifier(var, v, 2)) {flag = true;}
				}	
			}				
		}
		return flag;
	}

}
