package analysis.evolutions;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class GeneratorPDF {
	List<Change> changes = new ArrayList<>();
	Document document = new Document();
	public GeneratorPDF(List<Change> changes) {
		// TODO Auto-generated constructor stub
		this.changes = changes;
		openPDF();
	}
	
	
	public void openPDF() {
		try {
			Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLDITALIC);
	        Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);
            PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\raiza arteman\\Documents\\MESTRADO\\analises\\Report.pdf"));
            document.open();
            document.addAuthor("Raiza Artemam de Oliveira");
            Paragraph title = new Paragraph("Relat�rio de Mudan�as", chapterFont); 
            title.setAlignment(Element.ALIGN_CENTER);
            document.add(title);
            int m;
            if(changes.size() > 1) {
            	int lenght =  changes.size()-1;
            	document.add( Chunk.NEWLINE );
            	document.add(new Paragraph("Function Changes: "+changes.get(lenght).getFunction_changes()));
            	document.add(new Paragraph("Variable Changes: "+changes.get(lenght).getVariable_changes()));
            	document.add(new Paragraph("Type Changes: "+changes.get(lenght).getType_changes()));
            	document.add(new Paragraph("Modifier Changes: "+changes.get(lenght).getModifier_changes()));
            	document.add(new Paragraph("Qualifier Changes: "+changes.get(lenght).getQualifier_changes()));
            	document.add(new Paragraph("Specifier Changes: "+changes.get(lenght).getSpecifier_changes()));
            	
            	document.add(new Paragraph("Parameters Type Changes: "+changes.get(lenght).getParameters_type_changes()));
            	document.add(new Paragraph("Parameters Modifier Changes: "+changes.get(lenght).getParameters_modifier_changes()));
            	document.add(new Paragraph("Parameters Qualifier Changes: "+changes.get(lenght).getParameters_qualifier_changes()));
            	document.add(new Paragraph("Parameters Number Changes: "+changes.get(lenght).getParameters_qtd_changes()));
            }
            for(m = 0; m < changes.size(); m++) {
            	document.add( Chunk.NEWLINE );
            	document.add(new Paragraph("Dependency: "+changes.get(m).getIdDependency(), paragraphFont));
    			document.add(new Paragraph("Link Elements: "+changes.get(m).getDependency(), paragraphFont));
    			for(String tp: changes.get(m).get_type()) {
    				document.add(new Paragraph(tp));
    			}
    			for(String tp: changes.get(m).get_modifier()) {
    				document.add(new Paragraph(tp));
    			}
    			for(String tp: changes.get(m).get_qualifier()) {
    				document.add(new Paragraph(tp));
    			}
    			for(String tp: changes.get(m).get_specifier()) {
    				document.add(new Paragraph(tp));
    			}
            }
        }
        catch(DocumentException de) {
            System.err.println(de.getMessage());
        }
        catch(IOException ioe) {
            System.err.println(ioe.getMessage());
        }
		
		closePDF();
	}

	 public void closePDF() {
		 document.close();
    }   
}
