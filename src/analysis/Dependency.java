package analysis;

import java.util.ArrayList;
import java.util.List;

public class Dependency {
	
	public Variability variability1, variability2;
	public List<Link> linksFromThisDependency = new ArrayList<Link>();;
	
	int numberOfTimesChanged = 0;
	
}
