package analysis;

import java.util.ArrayList;
import java.util.List;

import analysis.core.Function;
import tree.Variable;

public class Variability {
	private String name;
	
	//funcoes
	private List<Function> declaredFunctionsInThisVariability = new ArrayList<Function>();
	private List<Function> usedFunctionsInThisVariability = new ArrayList<Function>();
	
	//variaveis
	private List<Variable> declaredGlobalVariablesInThisVariability = new ArrayList<Variable>();
	private List<Variable> usedGlobalVariablesInThisVariability = new ArrayList<Variable>();
	
	//elementos que a variabilidade fornece s�o os elementos declarados
	//elementos que a variabilidade requer s�o os elementos usados (dependencia)
	
	//metrics
	public int numberOfFunctions = declaredFunctionsInThisVariability.size() + usedFunctionsInThisVariability.size();
	public int numberOfVariables = declaredGlobalVariablesInThisVariability.size() + usedGlobalVariablesInThisVariability.size();
	
	public Variability(String name){
		this.name = name;
	}
	
	public List<Function> getDeclaredFunctionsInThisVariability(){
		return declaredFunctionsInThisVariability;
	}
	
	public List<Function> getUsedFunctionInThisVariability(){
		return usedFunctionsInThisVariability;
	}
	
	public List<Variable> getDeclaredGlobalVariablesInThisVariability(){
		return declaredGlobalVariablesInThisVariability;
	}
	
	public List<Variable> getUsedGlobalVariablesInThisVariability(){
		return usedGlobalVariablesInThisVariability;
	}
	
	public String getName(){
		return name;
	}
	
	public void addFunctionDeclarationVariability(Function f){
		declaredFunctionsInThisVariability.add(f);
	}
	
	public void addFunctionUseVariability(Function f){
		usedFunctionsInThisVariability.add(f);
	}
	
	public void addGlobalVariableDeclarationVariability(Variable var){
		declaredGlobalVariablesInThisVariability.add(var);
	}
	
	public void addGlobalVariableUseVariability(Variable var){
		usedGlobalVariablesInThisVariability.add(var);
	}
	
	public void printAllDeclarationsFromThisVariability(){
		System.out.println(this.name + ": ");
		System.out.print("funcoes:");
		for(Function f : declaredFunctionsInThisVariability){
			System.out.print(f.getName() + " ");
		}
		System.out.println();
		System.out.print("variaveis globais:");
		for(Variable var : declaredGlobalVariablesInThisVariability){
			System.out.print(var.getName() + " ");
		}
		System.out.println();
	}
	
	
}
