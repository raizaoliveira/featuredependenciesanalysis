package analysis;

import analysis.core.ProgramElement;
import analysis.core.Function;

public class Link {
	public ProgramElement caller;
	public ProgramElement callee;
	public int weight = 0;
	public boolean unique = false;
	
	public Link(ProgramElement caller, ProgramElement callee){
		this.caller = caller;
		this.callee = callee;
	}
	
	public ProgramElement  get_caller() {
		return caller;	
	}
	
	public ProgramElement get_callee() {
		return callee;
	}
	
	  @Override
	   public boolean equals(Object o) {
	      boolean result = false;
	      if ((this.get_caller().getName()).compareTo(((Link)o).get_caller().getName()) == 0) {
	    	  if((this.get_callee()).getName().compareTo(((Link)o).get_callee().getName()) == 0) {
	    		  result = true;
	    	  }
	      }
	      return result;
	   }   
	
	
}
