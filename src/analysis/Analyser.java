package analysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jgit.errors.SymlinksNotSupportedException;

import analysis.core.Function;
import metrics.Metrics;
import tree.FunctionCall;
import tree.FunctionDef;
import tree.Opt;
import tree.Variable;

public class Analyser {

	List<Variability> variabilities = new ArrayList<Variability>();
	List<Link> links = new ArrayList<Link>();
	VariabilityDependency dependencies = new VariabilityDependency();
	
	public static int numberOfVariabilities = 0;
	public static int numberOfDependencies = 0;
	
	public void setDps(Map<FunctionDef, Function> functions, List<Variable> globals, List<Variable> usoDeGlobals){
		System.out.println("-----");
		variabilities.add(new Variability("True")); //coloca como primeira variabilidade sendo TRUE
		//defining functions
		for (Entry<FunctionDef, Function> functionMap : functions.entrySet()) {
			Function f = functionMap.getValue();	
			setFunctionDeclarations(f);
		}
		//defining global variables
		for(Variable var : globals){
			setVariableDeclarations(var);
		}
		for(Variable var : usoDeGlobals){
			setVariableUses(var);
		}
		//System.out.println("USO EM TRUE:");

		setLinks();

		
		Metrics.numberOfVariabilities = numberOfVariabilities = variabilities.size();
		Metrics.numberOfDependencies = numberOfDependencies = dependencies.dependencies.size();
		Metrics.allLinksFromDependenciesOfAllVersions.add(dependencies.getAllLinksFromDps());
		Metrics.allDependenciesAndLinksDefined.add(setDependenciesObjects());

	}
		
	public void setFunctionDeclarations(Function f){
		String presenceCondition = "";

		boolean adicionou = false;
		for(Opt opt : f.getDirectives()){
			presenceCondition = opt.getPresenceCondition().toString();
			//verificando se a dependencia j� existe na lista
			for(Variability var : variabilities){
				if(var.getName().equals(presenceCondition)){
					
					var.addFunctionDeclarationVariability(f);
					return;
				}
			}
			Variability var = new Variability(presenceCondition);
			var.addFunctionDeclarationVariability(f);
			variabilities.add(var);
			adicionou = true;
		}
		if(!adicionou){
			variabilities.get(0).addFunctionDeclarationVariability(f);
		}
	}
	
	public List<Dependency> setDependenciesObjects(){
		List<Dependency> dependenciesListOfDependencies = new ArrayList<Dependency>();
		for(Link link : dependencies.getAllLinksFromDps()){
			Variable v;
			Function f;
			Dependency dp = new Dependency();
			if(link.caller.id == ID.Variable){
				dp.variability1 = getVariability(link.caller.getPresenceCondition().toString());
			}
			else if(link.caller.id == ID.Function){
				f = (Function) link.caller;
				boolean entrou = false;
				for(Opt opt : f.getDirectives()){
					entrou = true;
					dp.variability1 = getVariability(opt.getPresenceCondition().toString());
				}
				if(!entrou){
					dp.variability1 = getVariability("True");
				}
			}
			
			if(link.callee.id == ID.Variable){
				dp.variability2 = getVariability(link.callee.getPresenceCondition().toString());
			}
			else if(link.callee.id == ID.Function){
				f = (Function) link.callee;
				boolean entrou = false;
				for(Opt opt : f.getDirectives()){
					entrou = true;
					dp.variability2 = getVariability(opt.getPresenceCondition().toString());
				}
				if(!entrou){
					dp.variability2 = getVariability("True");
				}
			}
			
			Dependency foundDependency = dependencyAlreadyExists(dependenciesListOfDependencies, dp);
			if(foundDependency == null){
				dp.linksFromThisDependency.add(link);
				dependenciesListOfDependencies.add(dp);
			}
			else{
				foundDependency.linksFromThisDependency.add(link);
			}
			
		}
		return dependenciesListOfDependencies;
	}
	
	public Dependency dependencyAlreadyExists(List<Dependency> dps, Dependency dp){
		for(Dependency dpInTheList : dps){
			Variability v1 = dpInTheList.variability1;
			Variability v2 = dpInTheList.variability2;
			if(v1.getName().equals(dp.variability1.getName()) || v1.getName().equals(dp.variability2.getName())){
				if(v2.getName().equals(dp.variability1.getName()) || v2.getName().equals(dp.variability2.getName())){
					return dpInTheList;
				}
			}
		}
		return null;
	}
	
	public Variability getVariability(String presenceCondition){
		for(Variability var : variabilities){
			if(var.getName().equals(presenceCondition)){
				return var;
			}
		}
		return new Variability(presenceCondition);
	}
	
	public void setLinks(){
		for(Variability var : variabilities){
			System.out.println("variability "+var.getName());
			for(Function f : var.getDeclaredFunctionsInThisVariability()){
				for(FunctionCall fc : f.getFunctionCalls()){
					Link linkFunction = setLinksForFunctions(f, fc);
					//add links for variables
					variableDependencies(f);
					for(Opt opt : f.getDirectives()){
						functionDependencies(f, fc, opt, linkFunction);
					}
				}
			}
			
			
			for(Variable v : var.getDeclaredGlobalVariablesInThisVariability()) {
				for(Variability var2 : variabilities) {
					for(Variable variable : var2.getUsedGlobalVariablesInThisVariability()) {
						if (v.getName().compareTo(variable.getName()) == 0) {
							//System.out.println(variable.getPresenceCondition()+" declarada em "+var.getName()+" usada em "+var2.getName()+" -- "+v.getPresenceCondition());
							Link linkVariable = setLinksForVariable(v, variable);
							if(linkVariable.caller.getPresenceCondition().toString().equals("True") &&
									linkVariable.callee.getPresenceCondition().toString().equals("True")){
								break;
							}
								
							dependencies.add(linkVariable);
						}
					}
					
				}
				
			}
			

		}
	}
	
	
	public Link setLinksForVariable(Variable v, Variable vc){
		v.id = ID.Variable;
		vc.id = ID.Variable;
		Link link = new Link(vc,v);
		links.add(link);
		return link;
	}
	
	
	public Link setLinksForFunctions(Function f, FunctionCall fc){

		f.id = ID.Function;
		fc.getCaller().id = ID.Function;
		Link link = new Link(fc.getCaller(),f);
		links.add(link);
		return link;
	}
	
	public void functionDependencies(Function f,FunctionCall fc, Opt opt, Link link){
		
		//checando por dependencia
		String presenceConditionFunctionCaller = fc.getPresenceCondition().toString();
		String presenceConditionFunctionCallee = opt.getPresenceCondition().toString();

		if(!presenceConditionFunctionCaller.equals(presenceConditionFunctionCallee)){
			dependencies.add(link);
		}
	}
	
	public void variableDependencies(Function f){
		Set<Variable> localVariables = f.getLocalVariables();
		for(Variable variable : localVariables){
			variable.id = ID.Variable;
			Link link = new Link(f,variable);
			links.add(link);
			
			for(Opt opt : f.getDirectives()){
				String presenceConditionFunctionCaller = opt.getPresenceCondition().toString();
				String presenceConditionVariableCallee = variable.getPresenceCondition().toString();
				
				
				
				//checando por dependencia
				if(!presenceConditionFunctionCaller.equals(presenceConditionVariableCallee)){
					dependencies.add(link);
				}
			}
		
			
		}
	}
	
	public void setVariableDeclarations(Variable v){
		boolean adicionou = false;
		String presenceCondition = "";
		for(Variability var : variabilities){
			System.out.println("declaration "+ v.getPresenceCondition().toString());
			presenceCondition = v.getPresenceCondition().toString();
			if(var.getName().equals(presenceCondition)){
				var.addGlobalVariableDeclarationVariability(v);
				return;
			}
		}
		
		Variability var = new Variability(presenceCondition);
		var.addGlobalVariableDeclarationVariability(v);;
		variabilities.add(var);
		adicionou = true;
		
		if(!adicionou){
			variabilities.get(0).addGlobalVariableDeclarationVariability(v);
		}
	}
	
	public void setVariableUses(Variable v){
		System.out.println("set variables use");
		boolean adicionou = false;
		String presenceCondition = "";
		for(Variability var : variabilities){
			System.out.println("set variables use  "+v.getPresenceCondition().toString());
			presenceCondition = v.getPresenceCondition().toString();
			if(var.getName().equals(presenceCondition)){
				var.addGlobalVariableUseVariability(v);
				return;
			}
		}
		
		Variability var = new Variability(presenceCondition);
		var.addGlobalVariableUseVariability(v);
		variabilities.add(var);
		adicionou = true;
		
		if(!adicionou){
			variabilities.get(0).addGlobalVariableUseVariability(v);
		}
	}
	
	public void showLinks(){
		System.out.println("\nLINKS:");
		for(Link link : links){
			Variable v;
			Function f;
			if(link.caller.id == ID.Variable){
				v = (Variable) link.caller;
				System.out.print(v.getName() + " " + v.getPresenceCondition() + " usa ");
			}
			else if(link.caller.id == ID.Function){
				f = (Function) link.caller;
				boolean entrou = false;
				for(Opt opt : f.getDirectives()){
					entrou = true;
					System.out.print(f.getName() + " " + opt.getPresenceCondition() + " usa ");
				}
				if(!entrou){
					System.out.print(f.getName() + " True " + "usa ");
				}
				//se n�o entrou, ent�o quer dizer que a fun��o n�o tem diretivas e � declarada em True
				
			}
			
			if(link.callee.id == ID.Variable){
				v = (Variable) link.callee;
				System.out.print(v.getName() + " " + v.getPresenceCondition());
			}
			else if(link.callee.id == ID.Function){
				f = (Function) link.callee;
				boolean entrou = false;
				for(Opt opt : f.getDirectives()){
					entrou = true;
					System.out.print(f.getName() + " " + opt.getPresenceCondition());
				}
				if(!entrou){
					System.out.print(f.getName() + " True ");
				}
			}
			System.out.println();
		}
	}
	
	public void showVariableDependencies(){
		System.out.println("\nVARIABLE DEPENDENCIES:");
		for(Variability var : variabilities){
			System.out.print(var.getName() + ": ");
			for(Variable v : var.getDeclaredGlobalVariablesInThisVariability()){
				System.out.print(v.getName() + " ");
			}
			System.out.println();
			for(Variable v : var.getUsedGlobalVariablesInThisVariability()){
				System.out.print(v.getName()+ " ");
			}
			System.out.println();
		}
	}
	
	public void showDependencies() throws IOException{
		System.out.println("\nDEPENDENCIES:");
		for(Link link: dependencies.getAllLinksFromDps()){
			Variable v;
			Function f;
			if(link.caller.id == ID.Variable){
				v = (Variable) link.caller;
				System.out.print(v.getName() + " " + v.getPresenceCondition() + " usa ");
			}
			else if(link.caller.id == ID.Function){
				f = (Function) link.caller;
				boolean entrou = false;
				for(Opt opt : f.getDirectives()){
					entrou = true;
					System.out.print(f.getName() + " " + opt.getPresenceCondition() + " usa ");
				}
				if(!entrou){
					System.out.print(f.getName() + " True " + "usa ");
				}
				//se n�o entrou, ent�o quer dizer que a fun��o n�o tem diretivas e � declarada em True
				
			}
			
			if(link.callee.id == ID.Variable){
				v = (Variable) link.callee;
				System.out.print(v.getName() + " " + v.getPresenceCondition());
			}
			else if(link.callee.id == ID.Function){
				f = (Function) link.callee;
				boolean entrou = false;
				for(Opt opt : f.getDirectives()){
					entrou = true;
					System.out.print(f.getName() + " " + opt.getPresenceCondition());
				}
				if(!entrou){
					System.out.print(f.getName() + " True ");
				}
			}
			System.out.println();
		}
	}
	
	//nao esta sendo usado
	public void setFuctionUses(Function f){
		String presenceCondition = "";
		for(FunctionCall fc : f.getFunctionCalls()){
			presenceCondition = fc.getPresenceCondition().toString();
			//verificando se a dependencia j� existe na lista
			for(Variability var : variabilities){
				if(var.getName().equals(presenceCondition)){
					var.addFunctionUseVariability(f);
					return;
				}
			}

			Variability variability = new Variability(presenceCondition);
			variability.addFunctionUseVariability(f);
			variabilities.add(variability);

		}
	}
	
	public List<Link> getLinks(){
		return links;
	}
	
	public List<Link> getdependencies(){
		return links;
	}
}
