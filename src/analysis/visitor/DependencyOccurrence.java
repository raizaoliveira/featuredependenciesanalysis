package analysis.visitor;

import tree.Variable;
import de.fosd.typechef.featureexpr.FeatureExpr;

public class DependencyOccurrence {
	private FeatureExpr innerPresenceCondition;
	private Variable id;
	
	public DependencyOccurrence(FeatureExpr innerPresenceCondition, Variable id) {
		this.innerPresenceCondition = innerPresenceCondition;
		this.id = id;
	}

	public FeatureExpr getInnerPresenceCondition() {
		return innerPresenceCondition;
	}

	public Variable getId() {
		return id;
	}
}
