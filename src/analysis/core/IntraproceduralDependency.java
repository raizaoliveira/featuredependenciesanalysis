package analysis.core;

import tree.Variable;
import de.fosd.typechef.featureexpr.FeatureExpr;

public class IntraproceduralDependency {
	private Function function;
	private FeatureExpr presenceCondition;
	private Variable definition;
	private Variable use;
	
	public IntraproceduralDependency(Function function, FeatureExpr presenceCondition, Variable definition, Variable use) {
		this.function = function;
		this.presenceCondition = presenceCondition;
		this.definition = definition;
		this.use = use;
	}
	
	
	public IntraproceduralDependency(Function function, FeatureExpr presenceCondition, Variable definition) {
		this.function = function;
		this.presenceCondition = presenceCondition;
		this.definition = definition;
		this.use = null;
	}
	
	
	public Function getFunction() {
		return function;
	}
	
	public FeatureExpr getPresenceCondition() {
		return presenceCondition;
	}
	
	public Variable getDefinition() {
		return definition;
	}
	
	public Variable getUse() {
		return use;
	}
	
	public String getDirection() {
		if (definition.getPresenceCondition().isTautology()) {
			return "Mandatory<->Optional";
		} else if(use == null){
			return "No use";
		} else if (use.getPresenceCondition().isTautology()) {
			return "Optional<->Mandatory";
		} else {
			return "Optional<->Optional";
		}
	}
	
	public String getFile() {
		return this.definition.getPositionFrom().getFile().substring(5);
	}
	
	public String getDefinitionPosition() {
		return this.definition.getPositionFrom().getLine() + ":" + this.definition.getPositionFrom().getColumn();
	}
	
	public String getUsePosition() {
		if(use == null){
			return "No use" + ":" + this.definition.getPositionFrom().getColumn();
		}
		return this.use.getPositionFrom().getLine() + ":" + this.definition.getPositionFrom().getColumn();
	}
	
	public String toString() {
		String returnString = 
			getDirection() + ";" +
			getClass().getSimpleName() + ";" +
			getPresenceCondition().toString() + ";" +
			definition.getPresenceCondition().toString() + ";";
		if(use == null){
			returnString = returnString + "No use" + ";";
		}
		else{
			returnString = returnString + use.getPresenceCondition().toString() + ";";
		}
		returnString = returnString +
			function.getName() + ";" +
			definition.getName() + ";" +
			getFile() + ";" +
			getDefinitionPosition() + ";" +
			getUsePosition();
		
		return returnString;
	}
	
}
