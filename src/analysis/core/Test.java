package analysis.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.tools.ant.types.resources.selectors.InstanceOf;

import analysis.ID;
import analysis.Link;
import analysis.evolutions.Report;
import tree.Opt;
import tree.Variable;


public class Test {

	public static void main(String[] args) {
		long startTime = System.nanoTime();
		List<String> folders = new ArrayList<String>() {/**
			 * 
			 */
			private static final long serialVersionUID = 5257000321721251688L;

		{
			add("test/test2");
			add("test/test8");
		}};
		List<Project> projects = new ArrayList<>();
		Project oldversion;
		Project newversion;
		for (String folder : folders) {
			Project proj;
			File platform = new File("C:\\Users\\raiza arteman\\Documents\\MESTRADO\\intellij\\analyzer\\"+folder+"\\platform.h");
			if (platform.length() == 0) {
				proj = new Project("C:\\Users\\raiza arteman\\Documents\\MESTRADO\\intellij\\analyzer\\" + folder + "\\analysis", "" + folder + "\\include\\stubs.h");
			} else {
				proj = new Project("C:\\Users\\raiza arteman\\Documents\\MESTRADO\\intellij\\analyzer\\"+ folder+"\\analysis", "C:\\Users\\raiza arteman\\Documents\\MESTRADO\\intellij\\analyzer\\"+ folder +"\\platform.h");
			}
			
			projects.add(proj);
		}
		oldversion = projects.get(0);
		newversion = projects.get(1);
		
		
		oldversion.analyze();
		newversion.analyze();
		
		System.out.println(oldversion.getdependencies().size());
		System.out.println(newversion.getdependencies().size());
		
		Report out = new Report(oldversion, newversion);
		
		long elapsedTime = System.nanoTime() - startTime;
		System.out.println("Analysis ended in " + TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS) + " seconds.");
	}
	
	
	}
