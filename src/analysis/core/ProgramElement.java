package analysis.core;

import java.util.List;

import analysis.ID;
import tree.Node;
import tree.visitor.Visitor;

public class ProgramElement extends Node{
	public String file;
	private String type, qualificator, specifier, modifier;
	private String name;
	public ID id;
	
	//uses
	List<Function> programElementsUsed;
	
	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	public String getQualificator() {
		return qualificator;
	}

	public void setQualifier(String qualificator) {
		this.qualificator = qualificator;
	}

	public String getSpecifier() {
		return specifier;
	}

	public void setSpecifier(String specifier) {
		this.specifier = specifier;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

}
