package analysis.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tree.AtomicNamedDeclarator;
import tree.FunctionCall;
import tree.FunctionDef;
import tree.Variable;
import tree.Opt;

public class Function extends ProgramElement{
	private FunctionDef functionDef;
	private Set<Variable> localVariables = new HashSet<Variable>();
	private List<Variable> parameters = new ArrayList<Variable>();
	private List<FunctionCall> functionCallsItSelf = new ArrayList<FunctionCall>();
	private Set<Opt> directives = new HashSet<Opt>();
	private Set<Opt> opts = new HashSet<Opt>();

	public Function(FunctionDef functionDef, List<Variable> parameters) {
		this.functionDef = functionDef;
		this.parameters = parameters;
		
		for (int i = 0; i < functionDef.getChildren().size(); i++) {
			if (functionDef.getChildren().get(i) instanceof AtomicNamedDeclarator) {
				for (int j = 0; j < functionDef.getChildren().get(i).getChildren().size(); j++) {
					if (functionDef.getChildren().get(i).getChildren().get(j) instanceof Variable) {
						setName( ((Variable) functionDef.getChildren().get(i).getChildren().get(j)).getName());
						setType(((Variable) functionDef.getChildren().get(i).getChildren().get(j)).getType());
						setModifier(((Variable) functionDef.getChildren().get(i).getChildren().get(j)).getModifier());
						setSpecifier(((Variable) functionDef.getChildren().get(i).getChildren().get(j)).getSpecifier());
						setQualifier(((Variable) functionDef.getChildren().get(i).getChildren().get(j)).getQualificator());
						
					}
				}
			}
		}
	}
	
	public Function(FunctionDef functionDef) {
		this.functionDef = functionDef;
		
		for (int i = 0; i < functionDef.getChildren().size(); i++) {
			if (functionDef.getChildren().get(i) instanceof AtomicNamedDeclarator) {
				for (int j = 0; j < functionDef.getChildren().get(i).getChildren().size(); j++) {
					if (functionDef.getChildren().get(i).getChildren().get(j) instanceof Variable) {
						setName( ((Variable) functionDef.getChildren().get(i).getChildren().get(j)).getName());
					}
				}
			}
		}
	}

	public List<FunctionCall> getFunctionCalls() {
		return functionCallsItSelf;
	}
	
	public FunctionDef getFunctionDef() {
		return functionDef;
	}

	public void setFunctionDef(FunctionDef functionDef) {
		this.functionDef = functionDef;
	}

	

	public Set<Variable> getLocalVariables() {
		return localVariables;
	}

	public void setLocalVariables(Set<Variable> localVariables) {
		this.localVariables = localVariables;
	}
	
	public void setLocalVariables(List<Variable> localVariables) {
		this.localVariables = new HashSet<Variable>(localVariables);
	}

	public Set<Opt> getDirectives() {
		return directives;
	}

	public void setDirectives(Set<Opt> directives) {
		this.directives = directives;
	}
	
	public Set<Opt> getOpts() {
		return opts;
	}

	public void setOpts(Set<Opt> opts) {
		this.opts = opts;
	}
	
	public List<Variable> getParameters() {
		return parameters;
	}

	public void setParameters(List<Variable> parameters) {
		this.parameters = parameters;
	}
	
	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public void setName(String name) {
		super.setName(name);
	}
	
	@Override
	public String getType() {
		return super.getType();
	}

	@Override
	public void setType(String type) {
		super.setType(type);
	}
	
	@Override
	public String getQualificator() {
		return super.getQualificator();
	}

	@Override
	public void setQualifier(String qualificator) {
		super.setQualifier(qualificator);;
	}

	@Override
	public String getSpecifier() {
		return super.getSpecifier();
	}

	@Override
	public void setSpecifier(String specifier) {
		super.setSpecifier(specifier);;
	}

	@Override
	public String getModifier() {
		return super.getModifier();
	}

	@Override
	public void setModifier(String modifier) {
		super.setModifier(modifier);
	}


}
