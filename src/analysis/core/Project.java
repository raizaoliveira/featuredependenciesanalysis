package analysis.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import analysis.Analyser;
import analysis.Link;
import analysis.core.Ast.GenerationStatus;
import analysis.visitor.FindFunctionCallsVisitor;
import analysis.visitor.FindFunctionDefVisitor;
import analysis.visitor.FindFunctionDirectivesVisitor;
import analysis.visitor.FindFunctionParametersVisitor;
import analysis.visitor.FindFunctionsVisitor;
import analysis.visitor.FindGlobalVariableUsesVisitor;
import analysis.visitor.FindGlobalVariablesDeclarationsVisitor;
import analysis.visitor.FindVariableDeclarationVisitor;
import analysis.visitor.FindVariableDefinitionVisitor;
import analysis.visitor.FindVariableFunctionVisitor;
import analysis.visitor.FindVariableScopeVisitor;
import tree.FunctionCall;
import tree.FunctionDef;
import tree.Node;
import tree.Opt;
import tree.Variable;

public class Project {
	
	private String sourcePath;
	private String stubsPath;
	private int totalFiles = 0;
	private int totalSuccessfulFiles = 0;
	private Map<String, String> skippedFiles = new HashMap<String, String>();
	private Set<String> blacklist = new HashSet<String>();
	private List<Ast> asts = new ArrayList<Ast>();
	private Map<FunctionDef, Function> functions = new HashMap<FunctionDef, Function>();
	private List<IntraproceduralDependency> dependencies = new ArrayList<IntraproceduralDependency>();
	
	private List<Variable> globals = new ArrayList<Variable>();
	private List<Variable> usoDeGlobals = new ArrayList<Variable>();
	
	private List<Link> dependencieslist = new ArrayList<Link>();
	private Analyser dp = new Analyser();
	
	public Project(String sourcePath, String stubsPath) {
		setSourcePath(sourcePath);
		setStubsPath(stubsPath);
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getStubsPath() {
		return stubsPath;
	}

	public void setStubsPath(String stubsPath) {
		this.stubsPath = stubsPath;
	}
	
	public int getTotalFiles() {
		return totalFiles;
	}

	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}

	public int getTotalSuccessfulFiles() {
		return totalSuccessfulFiles;
	}

	public void setTotalSuccessfulFiles(int totalSuccessfulFiles) {
		this.totalSuccessfulFiles = totalSuccessfulFiles;
	}
	
	public Map<String, String> getSkippedFiles() {
		return skippedFiles;
	}

	public void setSkippedFiles(Map<String, String> skippedFiles) {
		this.skippedFiles = skippedFiles;
	}

	public Set<String> getBlacklist() {
		return blacklist;
	}

	public void setBlacklist(Set<String> blacklist) {
		this.blacklist = blacklist;
	}

	public List<Ast> getAsts() {
		return asts;
	}

	public void setAsts(List<Ast> asts) {
		this.asts = asts;
	}

	public Map<FunctionDef, Function> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<FunctionDef, Function> functions) {
		this.functions = functions;
	}
	
	public List<IntraproceduralDependency> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<IntraproceduralDependency> dependencies) {
		this.dependencies = dependencies;
	}
	
	public List<Variable> getGlobals() {
		return globals;
	}

	public void setGlobals(List<Variable> globals) {
		this.globals = globals;
	}
	
	public void setdependencies(){
		this.dependencieslist = dp.getdependencies();	
	}
	
	public List<Link> getdependencies(){
		return dependencieslist;	
	}

	public void getFile(File path) {
		if (path.isDirectory()) {
			for (File file : path.listFiles()) {
				getFile(file);
			}
		} else {
			if (!path.getName().startsWith(".")) {
				if (path.getName().endsWith(".c")) {
						//|| path.getName().endsWith(".h")) {
					System.out.println("FILE: " + path.getAbsolutePath());
					setTotalFiles(getTotalFiles() + 1);
					if (getBlacklist().contains(path.getPath())) {
						getSkippedFiles().put(path.getPath(), "File is blacklisted");
						System.out.println("File is blacklisted.");
						return;
					}
					Ast ast = new Ast(path, new File(getStubsPath()));
					GenerationStatus generationStatus;
					do {
						generationStatus = ast.generate();
					} while (generationStatus == GenerationStatus.OPTIONS_EXCEPTION);
					if (generationStatus == GenerationStatus.OK) {
						setTotalSuccessfulFiles(getTotalSuccessfulFiles() + 1);
						getAsts().add(ast);
					} else {
						getSkippedFiles().put(path.getPath(), generationStatus.toString());
					}
				}
			}
		}
	}
	
	public void loadBlacklist() {
		File f = new File(sourcePath + "..\\blacklist.txt");
		if (f.exists()) {
			try {
				setBlacklist(new HashSet<String>(FileUtils.readLines(f)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void analyze() {
		Map<FunctionDef, Function> functions = new HashMap<FunctionDef, Function>();
		// Get blacklisted files (files too messy to be parsed)
		loadBlacklist();
		// Parse every .c file in the path
		getFile(new File(sourcePath));
		// Cycle through every AST
		System.out.println("ASTs: " + asts.size());
		for (Ast ast : this.asts) {
			// Get all functions in this AST
			functions = findFunctions(ast);
			this.functions.putAll(functions);
			
	
			
			//Get global variables
			Node myAst = ast.getNode();
			int j = 1;
			FindGlobalVariablesDeclarationsVisitor findGlobalVariablesDeclarationsVisitor = new FindGlobalVariablesDeclarationsVisitor();
			myAst.accept(findGlobalVariablesDeclarationsVisitor);
			
			// Get all global variables on this AST
			List<Variable> globalVariablesDeclarations = findGlobalVariablesDeclarationsVisitor.getGlobalVariablesDeclarations();
			// Keep record of all global variables
			this.globals.addAll(globalVariablesDeclarations);
			
			for (Variable globalVariableDeclaration : globalVariablesDeclarations) {
				System.out.println("AST " + "/" + getAsts().size() + " - Global variable " + j++ + "/" + globalVariablesDeclarations.size());
				
				//nesse ponto do c�digo j� temos salvo a declara��o e a condi��o de cada vari�vel global
				//agora o ponto � registrar o uso
				
				FindGlobalVariableUsesVisitor findGlobalVariableUsesVisitor = new FindGlobalVariableUsesVisitor(globalVariableDeclaration);
				myAst.accept(findGlobalVariableUsesVisitor);
				// Get all uses for this global variable
				List<Variable> globalVariableUses = findGlobalVariableUsesVisitor.getGlobalVariableUses();
				
				for (Variable globalVariableUse : globalVariableUses) {
					
					FindVariableFunctionVisitor findVariableFunctionVisitor = new FindVariableFunctionVisitor();
					FindVariableScopeVisitor findVariableScopeVisitor = new FindVariableScopeVisitor();
					globalVariableUse.accept(findVariableFunctionVisitor);
					globalVariableUse.accept(findVariableScopeVisitor);
					usoDeGlobals.add(globalVariableUse);
				}
				
			}
			
		}
		
		findFunctionCalls();
		
		System.out.println("GLOBAL VARIABLES: ");
		for(Variable v : globals){
			System.out.println(v.getName() + ": " + v.getQualificator() + " " + v.getSpecifier() + " " + v.getModifier());
		}
		
		exportDirectives();
	}

	public void exportDirectives(){
		for (Entry<FunctionDef, Function> functionMap : getFunctions().entrySet()){
			Function f = functionMap.getValue();

			for(FunctionCall fc : f.getFunctionCalls()){
				System.out.println(fc.getPresenceCondition());
			}
		}
		
		dp.setDps(getFunctions(), globals, usoDeGlobals);
		setdependencies();
	}
	

	
	public Map<FunctionDef, Function> findFunctions(Ast ast) {
		Map<FunctionDef, Function> functions;
		
		Node myAst = ast.getNode();

		FindFunctionsVisitor findFunctionsVisitor = new FindFunctionsVisitor();
		myAst.accept(findFunctionsVisitor);
		// Get all function definitions on this AST
		List<FunctionDef> functionDefs = findFunctionsVisitor.getFunctions();
		functions = new HashMap<FunctionDef, Function>();
		// Create a function object for each function definition
		for (FunctionDef functionDef : functionDefs) {
			FindFunctionDirectivesVisitor findFunctionDirectivesVisitor = new FindFunctionDirectivesVisitor(functionDef);
			functionDef.accept(findFunctionDirectivesVisitor);
			// Get all function directives
			Set<Opt> functionDirectives = findFunctionDirectivesVisitor.getFunctionDirectives();
			// Get function inner directives (ifdefs inside function)
			Set<Opt> functionOpts = findFunctionDirectivesVisitor.getFunctionOpts();
			
			//setando parameters
			FindFunctionParametersVisitor findFunctionParametersVisitor = new FindFunctionParametersVisitor();
			functionDef.accept(findFunctionParametersVisitor);
			List<Variable> functionParameters = findFunctionParametersVisitor.getFunctionParameters();
			Function function = new Function(functionDef, functionParameters);
			function.setDirectives(functionDirectives);
			function.setOpts(functionOpts);
			
			//setando variables
			Set<Variable> variables = findVariableDeclarations(function);
			function.setLocalVariables(variables);

			functions.put(functionDef, function);
		}
		return functions;
	}
	
	public Set<Variable> findVariableDeclarations(Function function) {
		FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor();
		function.getFunctionDef().accept(findVariableDeclarationVisitor);
		Set<Variable> localVariables = findVariableDeclarationVisitor.getLocalVariables();
		function.setLocalVariables(localVariables);
		return localVariables;
	}
	
	public Set<Variable> findVariableDefinitions(Function function) {
		FindVariableDefinitionVisitor findVariableDefinitionVisitor = new FindVariableDefinitionVisitor();
		function.getFunctionDef().accept(findVariableDefinitionVisitor);
		Set<Variable> variablesDefinitions = findVariableDefinitionVisitor.getLocalVariables();
		Set<Variable> localVariablesDefinitions = new HashSet<Variable>();
		
		FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor();
		function.getFunctionDef().accept(findVariableDeclarationVisitor);
		Set<String> localVariablesDeclarations = findVariableDeclarationVisitor.getLocalVariablesNames();
		
		for (Variable variable : variablesDefinitions) {
			if (localVariablesDeclarations.contains(variable.getName())) {
				localVariablesDefinitions.add(variable);
			}
		}
		
		function.setLocalVariables(localVariablesDefinitions);
		return localVariablesDefinitions;
	}
	
	public void findFunctionCalls() {
		int i = 1;
		System.out.println("Looking for function calls on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			System.out.println("AST " + i + "/" + getAsts().size());
			Node myAst = ast.getNode();
			for (Entry<FunctionDef, Function> functionMap : getFunctions().entrySet()) {
				Function function = functionMap.getValue();
				FindFunctionCallsVisitor findFunctionCallsVisitor = new FindFunctionCallsVisitor(function.getFunctionDef());
				myAst.accept(findFunctionCallsVisitor);
			
				// Get all calls for this function
				List<FunctionCall> functionCalls = findFunctionCallsVisitor.getFunctionCalls();
				
				// set all callers for this function
				for(FunctionCall cs : functionCalls){
					FindFunctionDefVisitor findFunctionDefVisitor = new FindFunctionDefVisitor();
					cs.accept(findFunctionDefVisitor);
					Function caller = getFunctions().get(findFunctionDefVisitor.getFunctionDef());
					cs.setCaller(caller);
				}
				
				function.getFunctionCalls().addAll(functionCalls);
				
			}
		}
	}
	
}
