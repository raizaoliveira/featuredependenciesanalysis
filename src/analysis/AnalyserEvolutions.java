/*
 * create: raiza artemam 
 * date:17-01-2018
 * 
 * */
package analysis;

import analysis.core.Function;
import tree.FunctionCall;
import tree.FunctionDef;
import tree.Opt;
import tree.Variable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class AnalyserEvolutions extends Analyser{

	private List<Link> dependencieslist;

    public List<Link> getdependencies(){
		return dependencieslist;   	
    }
    
    public void setdependencies(){
		this.dependencieslist = dependencies.getAllLinksFromDps();
    	
    }
    public void setDps(Map<FunctionDef, Function> functions, List<Variable> globals, List<Variable> usoDeGlobals){
        System.out.println("-Set Deps-");
        variabilities.add(new Variability("True")); //coloca como primeira variabilidade sendo TRUE Pq?????????

        //defining functions
        for (Map.Entry<FunctionDef, Function> functionMap : functions.entrySet()) {
            Function f = functionMap.getValue();
            setFunctionDeclarations(f);
        }
        //defining global variables
        for(Variable var : globals){
            setVariableDeclarations(var);
        }
        for(Variable var : usoDeGlobals){
            setVariableUses(var);
        }
        setLinks();
        //showLinks();
        //showVariableDependencies();
        setdependencies();

    }

    public void showLinks(){
        String param_a = null, param_b = null;
        ArrayList<String> param_c = new ArrayList<>();
        String linkout = null;
        System.out.println("\nLINKS:");
        for(Link link : links){
            Variable v;
            Function f;
            if(link.caller.id == ID.Variable){
                v = (Variable) link.caller;
                System.out.println("v presence condition: "+v.getPresenceCondition().toString());
                param_a = v.getPresenceCondition().toString();
            }
            else if(link.caller.id == ID.Function){
                f = (Function) link.caller;
                boolean entrou = false;
                for(Opt opt : f.getDirectives()){
                    entrou = true;
                    param_a = opt.getPresenceCondition().toString();
                    System.out.println("f presence condition: "+opt.getPresenceCondition().toString());
                }
                if(!entrou){
                    param_a = f.getName();//mandatory
                    System.out.println("f presence condition: MANDATORY");
                }
                //se no entrou, ento quer dizer que a funo no tem diretivas e  declarada em True

            }

            if(link.callee.id == ID.Variable){
                v = (Variable) link.callee;
                param_b = v.getPresenceCondition().toString();
                param_c.add(v.getName());
            }
            else if(link.callee.id == ID.Function){
                f = (Function) link.callee;
                boolean entrou = false;
                for(Opt opt : f.getDirectives()){
                    entrou = true;
                    param_b = opt.getPresenceCondition().toString();
                    param_c.add(f.getName());
                    
                }
                if(!entrou){
                    param_b = f.getName();//"mandatory";
                    param_c.add(f.getName());
                }
            }
            for(int i =0; i < param_c.size(); i++){
                linkout = "<"+param_a+","+param_b+","+param_c.get(i)+">";
                System.out.println(linkout);
            }
            param_c.clear();

        }
    }


    public void showDependencies() throws IOException{
    	
		FileWriter fileW;
		File report = new File ("C:\\Users\\raiza arteman\\Documents\\MESTRADO\\analises\\analise.txt");//definindo o nome do locar do arquivo
		report.createNewFile ();
		fileW = new FileWriter (report);
		BufferedWriter buffW = new BufferedWriter (fileW);
		String caller, directives_caller ,callee,  directives_callee, category, type, modifier, specifier, qualifier;
		int  nparameters = -1;
		caller=directives_caller=callee=directives_callee=category=type=modifier=specifier=qualifier = "";
			
        System.out.println("\n aqui DEPENDENCIES:");
        for(Link link: dependencies.getAllLinksFromDps()){
            Variable v;
            Function f;
            if(link.caller.id == ID.Variable){
                v = (Variable) link.caller;
                System.out.print("139 "+v.getName() + " " + v.getPresenceCondition() + " usa ");
                caller = v.getName(); 
                directives_caller = v.getPresenceCondition().toString();
            }
            else if(link.caller.id == ID.Function){
                f = (Function) link.caller;
                boolean entrou = false;
                for(Opt opt : f.getDirectives()){
                    entrou = false;
                    System.out.print("146 "+f.getName() + " " + opt.getPresenceCondition() + " usa ");
                    caller = f.getName(); 
                    directives_caller = opt.getPresenceCondition().toString();
                }
                if(!entrou){
                    System.out.print("149 "+f.getName() + " True " + "usa ");
                    caller = f.getName(); 
                    directives_caller = "---";
                }
                //se nao entrou, entao quer dizer que a funcao nao tem diretivas e eh declarada em True

            }

            if(link.callee.id == ID.Variable){
                v = (Variable) link.callee;
                System.out.print("157 "+v.getName() + " " + v.getPresenceCondition());
                callee = v.getName();  
                directives_callee = v.getPresenceCondition().toString(); 
                category = "variable";
                type = v.getType();
                modifier = v.getModifier();
                specifier = v.getSpecifier(); 
                qualifier = v.getQualificator(); 
                nparameters = -1;
            }
            else if(link.callee.id == ID.Function){
                f = (Function) link.callee;
                boolean entrou = false;
               
                for(Opt opt : f.getDirectives()){
                    entrou = true;
                    System.out.print("164 "+f.getName() + " " + opt.getPresenceCondition()+" Return "+f.getType()+" parameters "+f.getParameters());
                    callee = f.getName();  
                    directives_callee = opt.getPresenceCondition().toString(); 
                    category = "function";
                    type = f.getType();
                    modifier = f.getModifier();
                    specifier = f.getSpecifier(); 
                    qualifier = f.getQualificator(); 
                    nparameters = f.getParameters().size();
                }
                if(!entrou){
                    System.out.print("167 "+f.getName() + " True ");
                    callee = f.getName();  
                    directives_callee = "---"; 
                    category = "function";
                    type = f.getType();
                    modifier = f.getModifier();
                    specifier = f.getSpecifier(); 
                    qualifier = f.getQualificator(); 
                    nparameters = f.getParameters().size();
                }
            }
            System.out.println(); 
			buffW.write (caller+","+directives_caller+","+callee+","+directives_callee+","+category+","+type+","+modifier+","+specifier+","+qualifier+","+nparameters);
			buffW.newLine ();//pula uma linha no arquivoescrever (result);
   
        }
        buffW.close();
        
    }
    

    
}
