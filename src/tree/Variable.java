package tree;

import analysis.core.ProgramElement;
import tree.visitor.Visitor;

public class Variable extends ProgramElement{
	@Override
	public String getType() {
		return super.getType();
	}

	@Override
	public void setType(String type) {
		super.setType(type);;
	}

	@Override
	public void setName(String name) {
		super.setName(name);
	}
	
	@Override
	public String getName() {
		return super.getName();
	}
	
	@Override
	public String getQualificator() {
		return super.getQualificator();
	}

	@Override
	public void setQualifier(String qualificator) {
		super.setQualifier(qualificator);;
	}

	@Override
	public String getSpecifier() {
		return super.getSpecifier();
	}

	@Override
	public void setSpecifier(String specifier) {
		super.setSpecifier(specifier);;
	}

	@Override
	public String getModifier() {
		return super.getModifier();
	}

	@Override
	public void setModifier(String modifier) {
		super.setModifier(modifier);
	}

	
	@Override
	public void accept(Visitor visitor) {
        visitor.run(this);    
    }
	
	@Override
	public boolean equals(Object obj) {
		if (this.getClass().getCanonicalName().equals(obj.getClass().getCanonicalName())){
			Node objNod = (Node) obj;
			
			if (objNod instanceof Variable){
				if (this instanceof Variable){
					if ( !((Variable)objNod).getName().equals(this.getName()) ){
						return false;
					}
				}
			}
			
		} else {
			return false;
		}
		return true;
	}
}
