package tree;

import analysis.core.Function;
import tree.visitor.Visitor;

public class FunctionCall extends Node {
	
	public Function caller;
	
	public Function getCaller(){
		return caller;
	}
	
	public void setCaller(Function caller){
		this.caller = caller;
	}
	
	@Override
	public void accept(Visitor visitor) {
		visitor.run(this);
	}

}
