package interprocedural;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import tree.FunctionCall;
import tree.TranslationUnit;
import tree.visitor.VisitorASTOrganizer;
import core.ASTGenerator;
import de.fosd.typechef.FrontendOptions;
import de.fosd.typechef.FrontendOptionsWithConfigFiles;
import de.fosd.typechef.Lex;
import de.fosd.typechef.lexer.options.OptionException;
import de.fosd.typechef.parser.TokenReader;
import de.fosd.typechef.parser.c.AST;
import de.fosd.typechef.parser.c.CParser;
import de.fosd.typechef.parser.c.CToken;
import de.fosd.typechef.parser.c.CTypeContext;
import de.fosd.typechef.parser.c.ParserMain;

public class ProjectAnalyzer {
	
	private Map<String, List<Dependency>> dependenciesMap = new HashMap<String, List<Dependency>>();
	private Map<String, List<PossibleDependency>> possibleDependenciesMap = new HashMap<String, List<PossibleDependency>>();
	private Map<String, List<Dependency>> probableDependenciesMap = new HashMap<String, List<Dependency>>();
	private Map<String, Map<String, FunctionMetrics>> functionMetricsMapMap = new HashMap<String, Map<String, FunctionMetrics>>();
	private Map<String, Map<String, Map<String, Integer>>> dependencyDepthsMap = new HashMap<String, Map<String, Map<String, Integer>>>();
	private Map<String, Function> orphanFunctionsMap = new HashMap<String, Function>();
	private String filePath;
	private String stubsPath;
	private List<File> files = new ArrayList<File>();
	private int analyzedFileCount = 0;
	
	public ProjectAnalyzer(String filePath, String stubsPath) {
		this.filePath = filePath;
		this.stubsPath = stubsPath;
	}

	public Map<String, List<Dependency>> getDependenciesMap() {
		return dependenciesMap;
	}
	
	public Map<String, List<PossibleDependency>> getPossibleDependenciesMap() {
		return possibleDependenciesMap;
	}
	
	public Map<String, List<Dependency>> getProbableDependenciesMap() {
		return probableDependenciesMap;
	}

	public Map<String, Map<String, FunctionMetrics>> getFunctionMetricsMap() {
		return functionMetricsMapMap;
	}
	
	public Map<String, Map<String, Map<String, Integer>>> getDependencyDepthsMap() {
		return dependencyDepthsMap;
	}	
	
	public Map<String, Function> getOrphanFunctionsMap() {
		return orphanFunctionsMap;
	}
	
	public String getFilePath() {
		return filePath;
	}
	
	public String getStubsPath() {
		return stubsPath;
	}
	
	public List<File> getFiles() {
		return files;
	}
	
	public int getAnalyzedFileCount() {
		return analyzedFileCount;
	}

	public static void main(String[] args) {
		ProjectAnalyzer projectAnalyzer;
		//FunctionCallSearch functionCallSearch;
		Spreadsheet m;
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\libssh0.5.3\\analysis\\src\\",
				"interprocedural\\libssh0.5.3\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\libssh.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\lighttpd\\src\\",
				"interprocedural\\lighttpd\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\lighttpd.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\sqlite\\analysis\\",
				"interprocedural\\sqlite\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\sqlite.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\apache\\analysis\\",
				"interprocedural\\apache\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\apache.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\clamav\\analysis\\",
				"interprocedural\\clamav\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\clamav.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\gzip1.2.4\\analysis\\",
				"interprocedural\\gzip1.2.4\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\gzip.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\ghostscript\\analysis\\",
				"interprocedural\\ghostscript\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\ghostscript.xls");
		*/
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\xterm2.9.1\\analysis\\",
				"interprocedural\\xterm2.9.1\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\xterm.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\bash2.01\\analysis\\",
				"interprocedural\\bash2.01\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\bash.xls");
		*/
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\berkeley\\analysis\\",
				"interprocedural\\berkeley\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\berkeley.xls");
		*/
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\cherokee\\include\\stubs.h");
		projectAnalyzer.getFiles(new File("interprocedural\\cherokee\\analysis"));
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\cherokee.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\bison2.0\\analysis\\",
				"interprocedural\\bison2.0\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\bison.xls");
		
		projectAnalyzer = new ProjectAnalyzer("interprocedural\\lua\\analysis\\",
				"interprocedural\\lua\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\lua.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\sendmail\\analysis\\",
				"interprocedural\\sendmail\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\sendmail.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\vim7.3\\analysis\\",
				"interprocedural\\vim7.3\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\vim.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\gnuplot\\analysis\\",
				"interprocedural\\gnuplot\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\gnuplot.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\mpsolve\\analysis\\",
				"interprocedural\\mpsolve\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\mpsolve.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\xfig3.2.3\\analysis\\",
				"interprocedural\\xfig3.2.3\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\xfig.xls");*/
		
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\libxml2\\analysis\\",
				"interprocedural\\libxml2\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\libxml.xls");*/
		/*projectAnalyzer = new ProjectAnalyzer("interprocedural\\libpng1.5.14\\analysis\\",
				"interprocedural\\libpng1.5.14\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\libpng.xls");*/
		projectAnalyzer = new ProjectAnalyzer("interprocedural\\dia0.97.2\\analysis\\",
				"interprocedural\\dia0.97.2\\include\\stubs.h");
		projectAnalyzer.analyze();
		m = new Spreadsheet(projectAnalyzer);
		m.createSpreadsheet("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\dia.xls");
	}
	
	public void analyze() {
		scanFiles(new File(getFilePath()));
		for (File file : getFiles()) {
			boolean success;
			do {
				success = checkFile(file);
			} while (!success);
		}
		for (File file : getFiles()) {
			boolean success;
			do {
				success = findOrphanFunctionsCalls(file);
			} while (!success);
		}
		
		for (Entry<String, List<PossibleDependency>> entry : getPossibleDependenciesMap().entrySet()) {
			for (PossibleDependency possibleDependency : entry.getValue()) {
				List<FunctionCall> functionCalls = orphanFunctionsMap.get(possibleDependency.getFunction().getName()).getFunctionCalls();
				for (FunctionCall functionCall : functionCalls) {
					if ((!(functionCall.getPresenceCondition().equivalentTo(possibleDependency.getPresenceCondition())) && // non equivalent presence condition
						(!(functionCall.getPresenceCondition().and(possibleDependency.getPresenceCondition()).isContradiction())))) { // presence condition in function call *and* parameter use cannot be a contradiction
						
						Dependency dependency = new FunctionCallDependency(possibleDependency.getFunction(), functionCall.getPresenceCondition().and(possibleDependency.getPresenceCondition()), possibleDependency.getPresenceCondition(), functionCall.getPresenceCondition(), possibleDependency.getVariable(), functionCall);
						if ((dependency.getInnerFile().equals(this.stubsPath)) || (dependency.getOuterFile().equals(this.stubsPath))) {
							continue;
						}
						if (probableDependenciesMap.containsKey(entry.getKey())) {
							probableDependenciesMap.get(entry.getKey()).add(dependency);
						} else {
							probableDependenciesMap.put(entry.getKey(), new ArrayList<Dependency>(Arrays.asList(dependency)));
						}
						//functionMetricsMap.get(dependency.getFunction().getName()).getProbableDependencies().add(dependency);
					}
				}
			}
		}
	}

	public void scanFiles(File path) {
		if (path.isDirectory()) {
			for (File file : path.listFiles()) {
				this.scanFiles(file);
			}
		} else {
			if (!path.getName().startsWith(".")) {
				if (path.getName().endsWith(".c")) {
						//|| path.getName().endsWith(".h")) {
					System.out.println("FILE: " + path.getAbsolutePath());
					//this.checkFile(path);
					files.add(path);
					//System.out.println("FIM!");
				}
			}
		}
	}

	public boolean checkFile(File file) {

		System.out.println("Checking " + file.getPath());
		
		TestInterProcedural testInterProcedural = new TestInterProcedural(file.getPath(), this.getStubsPath());

		try {
			testInterProcedural.refactorCode();
			getDependenciesMap().put(testInterProcedural.getFilePath(), testInterProcedural.getDependencies());
			getPossibleDependenciesMap().put(testInterProcedural.getFilePath(), testInterProcedural.getPossibleDependencies());
			getFunctionMetricsMap().put(testInterProcedural.getFilePath(), testInterProcedural.getFunctionMetricsMap());
			getDependencyDepthsMap().put(testInterProcedural.getFilePath(), testInterProcedural.getDependencyDepths());
			getOrphanFunctionsMap().putAll(testInterProcedural.getOrphanFunctionsMap());
		} catch (OptionException e) {
			e.printStackTrace();
			return false;
		}
		
		this.analyzedFileCount++;
		return true;
	}
	
	public boolean findOrphanFunctionsCalls(File file) {
		System.out.println("Scanning " + file.getPath() + " for function calls...");
		
		FrontendOptions myParserOptions = new FrontendOptionsWithConfigFiles();
		ArrayList<String> parameters = new ArrayList<String>();

		parameters.add("--lexNoStdout");
		parameters.add("-h");
		parameters.add(this.stubsPath);
		parameters.add(file.getPath());

		String[] parameterArray = parameters
				.toArray(new String[parameters.size()]);

		try {
			myParserOptions.parseOptions(parameterArray);
		} catch (OptionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		ParserMain parser = new ParserMain(new CParser(null, false));

		TokenReader<CToken, CTypeContext> in = Lex.lex(myParserOptions);

		AST ast = parser.parserMain(in, myParserOptions);
		tree.Node myAst = new TranslationUnit();
		new ASTGenerator().generate(ast, myAst);
		
		myAst.accept(new VisitorASTOrganizer());
		myAst.accept(new PresenceConditionVisitor());
		
		FindFunctionCallsVisitor findFunctionCallsVisitor = new FindFunctionCallsVisitor(getOrphanFunctionsMap());
		myAst.accept(findFunctionCallsVisitor);
		
		return true;
	}

}
